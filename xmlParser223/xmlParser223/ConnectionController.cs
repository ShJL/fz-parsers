﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.IO;
using System.Threading;

namespace xmlParser223
{
    public class ConnectionController
    {
        public class ConnectionObject
        {
            public int Num { get; set; }
            public string ConnString { get; set; }
            public NpgsqlConnection Conn => new NpgsqlConnection(ConnString);
            public bool IsFree { get; set; }
        }
        public static List<ConnectionObject> ConnList { get; set; } = new List<ConnectionObject>();



        public static void LoadConnList()
        {

            string connString = File.ReadAllText("connection.txt");
            for (int i = 1; i <= 70; i++)
            {
                ConnectionObject cObj = new ConnectionObject();
                cObj.Num = i;
                cObj.ConnString = connString;
                cObj.IsFree = true;
                ConnList.Add(cObj);
            }
        }

        private static readonly Random getrandom = new Random();
        private static readonly object syncLock = new object();
        public static int GetRandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return getrandom.Next(min, max);
            }
        }

        public static int GetFreeConnectionNum()
        {
            ConnectionObject conn = new ConnectionObject();



            int tryCnt = 100;
            while (tryCnt > 0)
            {
                lock (syncLock)
                {
                    var freeConns = ConnList.Where(s => s.IsFree).ToList();
                    if (freeConns.Count > 0)
                    {
                        int randomIndex = GetRandomNumber(0, freeConns.Count() - 1);
                        freeConns[randomIndex].IsFree = false;

                        return freeConns[randomIndex].Num;
                    }
                }
                tryCnt--;
                Thread.Sleep(300);
            }
            return 0;
        }

        public static ConnectionObject GetFreeConnectionObj()
        {




            int tryCnt = 10000;
            while (tryCnt > 0)
            {
                lock (syncLock)
                {
                    var freeConns = ConnList.Where(s => s.IsFree).ToList();
                    if (freeConns.Count > 0)
                    {
                        int randomIndex = GetRandomNumber(0, freeConns.Count() - 1);
                        freeConns[randomIndex].IsFree = false;

                        return freeConns[randomIndex];
                    }
                }
                tryCnt--;
                Thread.Sleep(1000);
            }
            return null;
        }


        public static void OpenFreeConn(out ConnectionObject cObj, out NpgsqlConnection conn)
        {

            cObj = GetFreeConnectionObj();
            conn = cObj.Conn;
            conn.Open();
        }
        public static void CloseBusyConn(ConnectionObject cObj, NpgsqlConnection conn)
        {
            conn.Close();
            cObj.IsFree = true;
        }








    }
}
