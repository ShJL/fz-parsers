﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace xmlParser223
{
    public class RV
    {
        public static string RVal(string test)
        {
            if (test != null)
                return test;
            else
                return "null";
        }

        public static string RSt(string test)
        {
            if (test != null)
                return $"'{test.Replace("'", "\"")}'";
            else
                return "null";
        }

        public static string RD(DateTime test)
        {
            if (test != new DateTime())
                return $"to_date('{test}', 'DD:MM:YYYY')";
            else
                return "null";
        }


        public static string RB(string test)
        {
            if (test != null)
                return $"{test}";
            else
                return "false";
        }

        public static string ChQ(string test)
        {
            if (test != null)
                return test.Replace("'", "\"");
            return test;
        }

        public static string ChS(string test)
        {
            if (test != null)
                return test.Replace(" ", "").Replace(",",".");
            return test;
        }

        public static int  RBooltoInt(bool test)
        {
            if (test == true)
                return 1;
            else
                return 0;
        }

        public static string RInt(int test)
        {
            if (test > 0)
                return $"{test}";
            else
                return "null";
        }


        public static string ParseInn(string test)
        {
            Regex rg = new Regex(@"\d{10}|\d{12}");
            string inn = rg.Match(test)?.Value;
            return inn;

        }



    }
}
