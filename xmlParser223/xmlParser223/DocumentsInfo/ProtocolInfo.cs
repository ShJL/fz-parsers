﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlParser223.DocumentsInfo
{
    public class ProtocolInfo
    {
        public string GUID { get; set; }
        public DateTime Date { get; set; }
        public string RegNum { get; set; }
        public int Version { get; set; }
        public string Type { set; get; }
        public string PurchaseRegNum { set; get; }
        public string PurchaseGUID { set; get; }
        public List<LotInfo> Lots { get; set; }
        public string MissedContest { get; set; }
        public string MissedReason { get; set; }
    }
}
