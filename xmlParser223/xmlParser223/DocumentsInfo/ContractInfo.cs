﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlParser223.DocumentsInfo
{
    public class ContractInfo
    {
        public string RegNum { get; set; }
        public string GUID { get; set; }
        public string Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PubDate { get; set; }
        public DateTime Date { get; set; }
        public DateTime ResumDate { get; set; }
        public string DocStatus { get; set; }
        public int Version { get; set; }
        public string ChangeContract { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string ModDescr { get; set; }
        public string HasSubcontractor { get; set; }
        public int LotNum { get; set; }
        public LotInfo Lot { get; set; }
        public CustomerInfo Customer { get; set; }
        public PurchaseInfo Purchase { get; set; }
        public ProtocolInfo Protocol { get; set; }
        public List<ContractPositionInfo> Positions { get; set; }
        

    }
}
