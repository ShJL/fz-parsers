﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlParser223.DocumentsInfo
{
    public class SupplierInfo
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Address { get; set; }
        public string INN { get; set; }
        public string KPP { get; set; }
        public string OGRN { get; set; }
        public string OKATO { get; set; }
        public string OKPO { get; set; }
        public string EMail { get; set; }
        public string OKPOF { get; set; }
        public string NonResident { get; set; }
        public string Code { get; set; }
        public string NRInfo { get; set; }
        public DateTime RegDate { get; set; }
        public string Offer { get; set; }
        public string Currency { get; set; }
        public string Accepted { get; set; }
        public string PreventReason { get; set; }
        public bool Winner { get; set; }
        public int Place { get; set; }
        public string AplNum { get; set; }
        



    }
}
