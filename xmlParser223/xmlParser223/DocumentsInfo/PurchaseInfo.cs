﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlParser223.DocumentsInfo
{
    public class PurchaseInfo
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string GUID { get; set; }
        public DateTime BirnDate { get; set; }
        public DateTime RefreshDate { get; set; }
        public DateTime StartApplicationDate { get; set; }
        public DateTime EndApplicationDate { get; set; }
        public string Type { get; set; }
        public string ChoiceWay { get; set; }
        public string ChoiseWayCode { get; set; }
        public string Region { get; set; }
        public string Stage { get; set; }
        public int Version { get; set; }
        public bool JointPurchase { get; set; }
        CustomerInfo Customer { get; set; }
        List<LotInfo> Lots { get; set; }
    }
}
