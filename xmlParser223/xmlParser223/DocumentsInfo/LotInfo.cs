﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlParser223.DocumentsInfo
{
    public class LotInfo
    {
        public string GUID { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public string NMC { get; set; }
        public string NMCInfo { get; set; }

        public bool JointPurchase { get; set; }
        public List<LotItemInfo> LotItems { get; set; }
        public List<CustomerInfo> Customers { get; set; }
        public List<SupplierInfo> Suppliers { get; set; }
    }
}
