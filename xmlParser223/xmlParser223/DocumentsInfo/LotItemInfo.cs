﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlParser223.DocumentsInfo
{
    public class LotItemInfo
    {
        public string GUID { get; set; }
        public int Number { get; set; }
        public string OKDP { set; get; }
        public string OKPD { set; get; }
        public string OKPD2 { set; get; }
        public string OKVED { set; get; }
        public string OKVED2 { set; get; }
        public string OKEI { set; get; }
        public string OKEIName { set; get; }
        public string Qnty { set; get; }
        public List<CustomerInfo> Customers { get; set; }
    }
}
