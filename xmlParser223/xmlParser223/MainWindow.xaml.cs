﻿using Microsoft.Win32;
using OfficeOpenXml;
using Microsoft.WindowsAPICodePack.Dialogs;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using xmlParser223.Archiever;
using xmlParser223.ParserXML;
using xmlParser223.Export;
using static xmlParser223.ConnectionController;
using static xmlParser223.ParserXML.LinkController;
using System.Xml.Linq;
using System.IO.Compression;

namespace xmlParser223
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        static void RunExProt(NpgsqlConnection conn)
        {
            string sql = "select id from file_status where file_code = 3 and load_status = 0 and id like 'purchaseP%';";
            var cmd = new NpgsqlCommand(sql, conn);
            Regex rg = new Regex("^[^_]+");

            NpgsqlDataReader reader = cmd.ExecuteReader();
            List<Task> taskList = new List<Task>();
            while (reader.Read())
            {
                string file = reader[0].ToString();
                string directory = rg.Match(file).Value;

                string fileName = System.IO.Path.Combine(@"C:\Users\asuvorin\source\repos\xmlParser223\xmlParser223\bin\Debug\FTP\xml\fz223\Moskva", directory, file);

                taskList.Add(Task.Factory.StartNew(() => ProtocolLoader.ParseProtocol(fileName)));

            }
            Task.WaitAll(taskList.ToArray());

        }

        static void RunParser(CommonOpenFileDialog dlg, NpgsqlConnection conn)
        {
            LoadConnList();

            string sql = $"insert into gz.load_session (id_type_session, type_param, timestamp_from) values (1, 'Region', to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS')) returning id;";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            int sessionID = Convert.ToInt32(cmd.ExecuteScalar());

            foreach (var region in dlg.FileNames)
            {
                Regex rg = new Regex(@"[^\\]+$");
                string regionName = rg.Match(region).Value;
                sql = $"select id from gz.region where ftp_name = '{regionName}'";
                cmd = new NpgsqlCommand(sql, conn);
                int regionId = (int)cmd.ExecuteScalar();

                cmd = new NpgsqlCommand($"insert into param_session values ({sessionID}, 'Region', '{regionName}') returning id;", conn);
                int idParam = (int)cmd.ExecuteScalar();

                cmd = new NpgsqlCommand($"insert into session_log (timestamp_from, id_session, id_step, id_param) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 3, {idParam}) returning id", conn);
                int idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    NoticeLoader.ParseNotices(region, conn, sessionID, regionId);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", conn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", conn);
                    cmd.ExecuteNonQuery();
                }

                cmd = new NpgsqlCommand($"insert into session_log (timestamp_from, id_session, id_step, id_param) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 4, {idParam}) returning id", conn);
                idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    ContractLoader.ParseContracts(region, conn, sessionID);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", conn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", conn);
                    cmd.ExecuteNonQuery();
                }


                cmd = new NpgsqlCommand($"insert into session_log (timestamp_from, id_session, id_step, id_param) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 5, {idParam}) returning id", conn);
                idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    ProtocolLoader.ParseProtocols(region, conn, sessionID, regionId);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", conn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", conn);
                    cmd.ExecuteNonQuery();
                }
                cmd = new NpgsqlCommand($"insert into session_log (timestamp_from, id_session, id_step, id_param) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 6, {idParam}) returning id", conn);
                idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    ContractCompletingLoader.ParseCompletings(region, sessionID, regionId);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", conn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", conn);
                    cmd.ExecuteNonQuery();
                }



            }
            ProtocolPurchaseLink(sessionID, conn);
            ParticipationLotLink(sessionID, conn);
            ContractPurchaseLink(sessionID, conn);
            LotContractLotLink(sessionID, conn);




        }



        private void MiRunReArchiver_Click(object sender, RoutedEventArgs e)
        {
            (sender as MenuItem).IsEnabled = false;

            var dlg = new CommonOpenFileDialog();

            dlg.Title = "Выбор папки региона с архивами";
            dlg.IsFolderPicker = true;
            dlg.InitialDirectory = Directory.GetCurrentDirectory();

            dlg.AddToMostRecentlyUsedList = false;
            dlg.AllowNonFileSystemItems = false;
            dlg.DefaultDirectory = Directory.GetCurrentDirectory();
            dlg.EnsureFileExists = true;
            dlg.EnsurePathExists = true;
            dlg.EnsureReadOnly = false;
            dlg.EnsureValidNames = true;
            dlg.Multiselect = true;
            dlg.ShowPlacesList = true;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
                foreach (var item in dlg.FileNames)
                {
                    ExtractController.ReAcrchive(item);
                }

            MessageBox.Show("Файлы разахивированы!");
            (sender as MenuItem).IsEnabled = true;
        }

        private void MiRunParser_Click(object sender, RoutedEventArgs e)
        {

            (sender as MenuItem).IsEnabled = false;

            var dlg = new CommonOpenFileDialog();

            dlg.Title = "Выбор папки региона с xml";
            dlg.IsFolderPicker = true;
            dlg.InitialDirectory = Directory.GetCurrentDirectory() + @"\FTP\xml";

            dlg.AddToMostRecentlyUsedList = false;
            dlg.AllowNonFileSystemItems = false;
            dlg.DefaultDirectory = Directory.GetCurrentDirectory();
            dlg.EnsureFileExists = true;
            dlg.EnsurePathExists = true;
            dlg.EnsureReadOnly = false;
            dlg.EnsureValidNames = true;
            dlg.Multiselect = true;
            dlg.ShowPlacesList = true;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                Stopwatch st = Stopwatch.StartNew();
                NpgsqlConnection conn = new NpgsqlConnection("Server=CBS-PG;Port=5432;User Id=gz_user;Password=gz_user;Database=postgres;");

                conn.Open();

                RunParser(dlg, conn);
                conn.Close();
                MessageBox.Show($"Файлы обработаны за {st.ElapsedMilliseconds / 1000.0} сек!");
            }

            (sender as MenuItem).IsEnabled = true;

        }

        private void MiExport_Click(object sender, RoutedEventArgs e)
        {
            (sender as MenuItem).IsEnabled = false;

            // Create a new instance of the Form2 class
            ExportForm settingsForm = new ExportForm();

            // Show the settings form
            settingsForm.Show();


            (sender as MenuItem).IsEnabled = true;

        }

        private void MiTest_Click(object sender, RoutedEventArgs e)
        {
            (sender as MenuItem).IsEnabled = false;
            LoadConnList();
            NpgsqlCommand cmd; ;
            int idSessionLog;
            string sql;

            OpenFreeConn(out var a, out var cn);


            for (int sessionID = 5; sessionID <= 10; sessionID++)
            {

                cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 7) returning id", cn);
                idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    ProtocolPurchaseLink(sessionID, cn);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }


                cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 8) returning id", cn);
                idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    ParticipationLotLink(sessionID, cn);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }


                cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 9) returning id", cn);
                idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    ContractPurchaseLink(sessionID, cn);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }


                cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 10) returning id", cn);
                idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    LotContractLotLink(sessionID, cn);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }


                sql = $"update gz.load_session set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), result = 'ok' where id = {sessionID};";
                cmd = new NpgsqlCommand(sql, cn);
                cmd.ExecuteNonQuery();


            }


            CloseBusyConn(a, cn);




            MessageBox.Show("Тест завершен!");

        }

        private void MiRunParserExFiles_Click(object sender, RoutedEventArgs e)
        {
            LoadConnList();
            ConnectionObject cObj = GetFreeConnectionObj();
            NpgsqlConnection conn = cObj.Conn;
            conn.Open();
            RunExProt(conn);
            conn.Close();
            MessageBox.Show("Ошибочные файлы обработаны");
        }

        private void MiRunParserZipFiles_Click(object sender, RoutedEventArgs e)
        {
            (sender as MenuItem).IsEnabled = false;
            LoadConnList();

            var dlg = new CommonOpenFileDialog();

            dlg.Title = "Выбор папки региона с архивами";
            dlg.IsFolderPicker = true;
            dlg.InitialDirectory = Directory.GetCurrentDirectory() + @"\FTP\zip";

            dlg.AddToMostRecentlyUsedList = false;
            dlg.AllowNonFileSystemItems = false;
            dlg.DefaultDirectory = Directory.GetCurrentDirectory();
            dlg.EnsureFileExists = true;
            dlg.EnsurePathExists = true;
            dlg.EnsureReadOnly = false;
            dlg.EnsureValidNames = true;
            dlg.Multiselect = true;
            dlg.ShowPlacesList = true;

            if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
            {
                Stopwatch st = Stopwatch.StartNew();


                RunParserArchive(dlg);


                MessageBox.Show($"Файлы обработаны за {st.Elapsed}!");
            }

            (sender as MenuItem).IsEnabled = true;
        }

        static void RunParserArchive(CommonOpenFileDialog dlg)
        {
            OpenFreeConn(out var a, out var cn);
            string sql = $"insert into gz.load_session (id_type_session, type_param, timestamp_from) values (1, 'Region', to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS')) returning id;";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, cn);
            int idSessionLog;
            int sessionID = (int)cmd.ExecuteScalar();

            foreach (var regionPath in dlg.FileNames)
            {
                Regex rg = new Regex(@"[^\\]+$");
                string regionName = rg.Match(regionPath).Value;
                sql = $"select id from gz.region where ftp_name = '{regionName}'";
                cmd = new NpgsqlCommand(sql, cn);
                int regionId = (int)cmd.ExecuteScalar();

                cmd = new NpgsqlCommand($"insert into param_session values ({sessionID}, 'Region', '{regionName}') returning id;", cn);
                int idParam = (int)cmd.ExecuteScalar();


                //cmd = new NpgsqlCommand($"insert into session_log (timestamp_from, id_session, id_step, id_param) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 3, {idParam}) returning id", cn);
                //idSessionLog = (int)cmd.ExecuteScalar();
                //try
                //{
                //    NoticeLoader.ParseZipNotices(regionPath, sessionID, regionId);
                //    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                //    cmd.ExecuteNonQuery();
                //}
                //catch (Exception err)
                //{
                //    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                //    cmd.ExecuteNonQuery();
                //    MessageBox.Show(err.Message);
                //    throw;
                //}

                //cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step, id_param) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 4, {idParam}) returning id", cn);
                //idSessionLog = (int)cmd.ExecuteScalar();
                //try
                //{
                //    ContractLoader.ParseZipContracts(regionPath, sessionID, regionId);
                //    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                //    cmd.ExecuteNonQuery();
                //}
                //catch (Exception err)
                //{
                //    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                //    cmd.ExecuteNonQuery();
                //    MessageBox.Show(err.Message);
                //    throw;
                //}

                cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step, id_param) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 5, {idParam}) returning id", cn);
                idSessionLog = (int)cmd.ExecuteScalar();
                try
                {
                    ProtocolLoader.ParseZipProtocols(regionPath, sessionID, regionId);
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show(err.Message);
                    throw;
                }

                //    cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step, id_param) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 6, {idParam}) returning id", cn);
                //    idSessionLog = (int)cmd.ExecuteScalar();
                //    try
                //    {
                //        ContractCompletingLoader.ParseZipCompletings(regionPath, sessionID, regionId);
                //        cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                //        cmd.ExecuteNonQuery();
                //    }
                //    catch (Exception err)
                //    {
                //        cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                //        cmd.ExecuteNonQuery();
                //        MessageBox.Show(err.Message);
                //        throw;
                //    }
            }

            cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 7) returning id", cn);
            idSessionLog = (int)cmd.ExecuteScalar();
            try
            {
                ProtocolPurchaseLink(sessionID, cn);
                cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                cmd.ExecuteNonQuery();
            }


            cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 8) returning id", cn);
            idSessionLog = (int)cmd.ExecuteScalar();
            try
            {
                ParticipationLotLink(sessionID, cn);
                cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
                cmd.ExecuteNonQuery();
            }


            //cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 9) returning id", cn);
            //idSessionLog = (int)cmd.ExecuteScalar();
            //try
            //{
            //    ContractPurchaseLink(sessionID, cn);
            //    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
            //    cmd.ExecuteNonQuery();
            //}
            //catch (Exception err)
            //{
            //    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
            //    cmd.ExecuteNonQuery();
            //}


            //cmd = new NpgsqlCommand($"insert into gz.session_log (timestamp_from, id_session, id_step) values (to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), {sessionID}, 10) returning id", cn);
            //idSessionLog = (int)cmd.ExecuteScalar();
            //try
            //{
            //    LotContractLotLink(sessionID, cn);
            //    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {idSessionLog};", cn);
            //    cmd.ExecuteNonQuery();
            //}
            //catch (Exception err)
            //{
            //    cmd = new NpgsqlCommand($"update session_log set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'Error', error = '{err.Message}' where id = {idSessionLog};", cn);
            //    cmd.ExecuteNonQuery();
            //}


            sql = $"update gz.load_session set timestamp_to = to_timestamp('{DateTime.Now}', 'DD.MM.YYYY HH24:MI:SS'), step_result = 'ok' where id = {sessionID};";
            cmd = new NpgsqlCommand(sql, cn);
            cmd.ExecuteNonQuery();

            CloseBusyConn(a, cn);
        }
    }
}
