﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using OfficeOpenXml;
using System.IO;

namespace xmlParser223
{
    public class ExportData
    {
        public static void ExportQuery(string sql, string fileName)
        {
            string connection = File.ReadAllText("connection.txt");
            string excelFileName;
            if (fileName == "")
                excelFileName = $"Exprort_{DateTime.Now.ToString().Replace(" ", "_").Replace(":", "").Replace(".", "")}.xlsx";
            else
            {
                excelFileName = fileName;
                if (File.Exists(fileName))
                    File.Delete(fileName);
            }
                
            FileInfo newFile = new FileInfo(excelFileName);
            NpgsqlConnection conn = new NpgsqlConnection(connection);

            conn.Open();

            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            NpgsqlDataReader reader = cmd.ExecuteReader();


            var columnList = reader.GetColumnSchema();
            int columnsCount = columnList.Count();
            int rowID = 2;


            using (ExcelPackage package = new ExcelPackage(newFile))
            {
                package.Workbook.Worksheets.Add("Result");
                ExcelWorksheet sheet = package.Workbook.Worksheets[1];
                WriteColumnsNames(columnList, sheet);
                while (reader.Read() && rowID < 1040877)
                {

                    for (int i = 0; i < columnsCount; i++)
                    {
                        sheet.Cells[rowID, i + 1].Value = reader[i];
                    }
                    rowID++;


                }

                package.Save();
            }



            conn.Close();
        }

        static void WriteColumnsNames(System.Collections.ObjectModel.ReadOnlyCollection<Npgsql.Schema.NpgsqlDbColumn> columnList, ExcelWorksheet sheet)
        {
            int columnID = 1;
            foreach (var column in columnList)
            {
                sheet.Cells[1, columnID].Value = column.ColumnName;
                columnID++;
            }
        }

        static void WriteData()
        {
        }

    }
}
