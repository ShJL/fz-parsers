﻿namespace xmlParser223.Export
{
    partial class ExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.sqlBox = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.saveFile = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(114, 13);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Введите SQL-запрос:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(236, 257);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Экспортировать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // sqlBox
            // 
            this.sqlBox.Location = new System.Drawing.Point(132, 12);
            this.sqlBox.Multiline = true;
            this.sqlBox.Name = "sqlBox";
            this.sqlBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sqlBox.Size = new System.Drawing.Size(468, 105);
            this.sqlBox.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(12, 151);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(114, 31);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "Выберите файл для сохранения:";
            // 
            // saveFile
            // 
            this.saveFile.Location = new System.Drawing.Point(132, 162);
            this.saveFile.Name = "saveFile";
            this.saveFile.Size = new System.Drawing.Size(391, 20);
            this.saveFile.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(529, 159);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Обзор";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 299);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.saveFile);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.sqlBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Name = "ExportForm";
            this.Text = "ExportForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox sqlBox;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox saveFile;
        private System.Windows.Forms.Button button2;
    }
}