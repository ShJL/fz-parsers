﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static xmlParser223.ConnectionController;

namespace xmlParser223.ParserXML
{
    public class LinkController
    {
        public static void ProtocolPurchaseLink(int snID, NpgsqlConnection conn)
        {
            string sql = $"select guid, reg_num_notice from gz.protocol where id_session = {snID}";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            NpgsqlDataReader reader = cmd.ExecuteReader();


            List<Task> taskList = new List<Task>();
            while (reader.Read())
            {
                string protGUID = reader[0].ToString();
                object prchNum = reader[1];
                taskList.Add(Task.Factory.StartNew(() =>
                {
                    int freeConnNum = GetFreeConnectionNum();
                    var cn = ConnList.Where(s => s.Num == freeConnNum).FirstOrDefault().Conn;
                    cn.Open();

                    string select = $"select id from gz.purchase where reg_num='{prchNum}' and version = (select max(version) as max_version from gz.purchase where reg_num = '{prchNum}' group by reg_num); ";

                    var com = new NpgsqlCommand(select, cn);
                    object prchID = com.ExecuteScalar();
                    com.Dispose();
                    if (prchID != null)
                    {
                        string update = $"update gz.protocol set id_purchase = {prchID} where guid = '{protGUID}'";
                        com = new NpgsqlCommand(update, cn);
                        com.ExecuteNonQuery();
                    }
                    cn.Close();
                    ConnList.Where(s => s.Num == freeConnNum).FirstOrDefault().IsFree = true;
                    //ConnectionClose(freeConnNum);
                }));
            }
            Task.WaitAll(taskList.ToArray());
            reader.Dispose();
            reader.Close();






        }


        public static void ParticipationLotLink(int snID, NpgsqlConnection conn)
        {
            string sql = $"select p.id, guid_lot, id_purchase, lot_num from gz.protocol pr join gz.participation p on pr.guid = p.guid_protocol" +
                $" where p.id_session = {snID}";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            NpgsqlDataReader reader = cmd.ExecuteReader();


            List<Task> taskList = new List<Task>();
            while (reader.Read())
            {
                int partId = (int)reader[0];
                string lotGUID = reader[1].ToString();
                string prchId = reader[2]?.ToString() ?? "";
                int lotNum = (int)reader[3];

                taskList.Add(Task.Factory.StartNew(() =>
                {
                    OpenFreeConn(out var a, out var cn);
                    string select = $"select id from gz.lot where guid = '{lotGUID}';";


                    var com = new NpgsqlCommand(select, cn);
                    object lotID = com.ExecuteScalar();
                    com.Dispose();
                    if (lotID != null)
                    {
                        string update = $"update gz.participation set id_lot = {lotID} where id = {partId}";
                        com = new NpgsqlCommand(update, cn);
                        com.ExecuteNonQuery();
                    }
                    else if (prchId != "")
                    {
                        select = $"select id from gz.lot where number = {lotNum} and id_purchase = {prchId}";
                        com = new NpgsqlCommand(select, cn);
                        lotID = com.ExecuteScalar();
                        com.Dispose();
                        if (lotID != null)
                        {
                            string update = $"update gz.participation set id_lot = {lotID} where id = {partId}";
                            com = new NpgsqlCommand(update, cn);
                            com.ExecuteNonQuery();
                        }
                    }
                    CloseBusyConn(a, cn);
                }));
            }
            Task.WaitAll(taskList.ToArray());

            reader.Dispose();
            reader.Close();
        }

        public static void ContractPurchaseLink(int snID, NpgsqlConnection conn)
        {
            string sql = $" select guid, guid_purchase, id_purchase from gz.contract where id_session = {snID}";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            NpgsqlDataReader reader = cmd.ExecuteReader();


            List<Task> taskList = new List<Task>();
            while (reader.Read())
            {
                string cGUID = reader[0].ToString();
                string prchGUID = reader[1]?.ToString() ?? "";
                string prePrchID = reader[2]?.ToString() ?? "";

                taskList.Add(Task.Factory.StartNew(() =>
                {
                    if (prchGUID != "" && prePrchID == "")
                    {
                        OpenFreeConn(out var a, out var cn);
                        string select = $"select id from gz.purchase where guid = '{prchGUID}';";
                        var com = new NpgsqlCommand(select, cn);
                        object prchID = com.ExecuteScalar();
                        com.Dispose();
                        if (prchID != null)
                        {
                            string update = $"update gz.contract set id_purchase = '{prchID}' where guid = '{cGUID}'";
                            com = new NpgsqlCommand(update, cn);
                            com.ExecuteNonQuery();
                        }
                        CloseBusyConn(a, cn);
                    }
                    
                }));
            }
            Task.WaitAll(taskList.ToArray());

            reader.Dispose();
            reader.Close();

        }

        public static void LotContractLotLink(int snID, NpgsqlConnection conn)
        {
            string sql = $" select lc.id, lc.guid_lot, c.id_purchase from gz.lot_contract lc join contract c " +
                $"on lc.guid_contract = c.guid where lc.id_session = {snID};";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            NpgsqlDataReader reader = cmd.ExecuteReader();


            List<Task> taskList = new List<Task>();
            while (reader.Read())
            {
                int lcID = (int)reader[0];
                string lotGuid = reader[1]?.ToString() ?? "";
                string prchID = reader[2]?.ToString() ?? "";

                taskList.Add(Task.Factory.StartNew(() =>
                {
                    if (prchID != "" && lotGuid != "")
                    {
                        OpenFreeConn(out var a, out var cn);
                        string select = $"select id from gz.lot where id_purchase = '{prchID}' and guid = '{lotGuid}';";
                        var com = new NpgsqlCommand(select, cn);
                        object lotID = com.ExecuteScalar();
                        com.Dispose();
                        if (lotID != null)
                        {
                            string update = $"update gz.lot_contract set id_lot = '{lotID}' where id = {lcID}";
                            com = new NpgsqlCommand(update, cn);
                            com.ExecuteNonQuery();
                        }
                        CloseBusyConn(a, cn);
                    }

                }));
            }
            Task.WaitAll(taskList.ToArray());

            reader.Dispose();
            reader.Close();

        }




    }
}
