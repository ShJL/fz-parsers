﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using xmlParser223.DocumentsInfo;
using static xmlParser223.ConnectionController;


namespace xmlParser223.ParserXML
{
    public class ContractLoader
    {
        static int SnID { get; set; }
        static int RegionID { get; set; }
        static object SyncLock { get; } = new object();
        static XName GetGlobalName(string prop) => XName.Get(prop, "http://zakupki.gov.ru/223fz/types/1");
        public static XName Getns2Name(string prop) => XName.Get(prop, "http://zakupki.gov.ru/223fz/contract/1");


        public static void ParseContracts(string path, NpgsqlConnection conn, int sID)
        {
            SnID = sID;
            List<string> directs = Directory.GetDirectories(path).Where(s => s.Contains("contract") && !s.Contains("Completing")).ToList();

            foreach (var directory in directs)
            {
                List<Task> taskList = new List<Task>();
                List<string> xmlFiles = Directory.GetFiles(directory).Reverse().ToList();
                foreach (var file in xmlFiles)
                    taskList.Add(Task.Factory.StartNew(() => ParseContractXML(file)));
                Task.WaitAll(taskList.ToArray());

            }

        }
        public static void ParseZipContracts(string path, int sID, int regionId)
        {
            SnID = sID;
            RegionID = regionId;

            List<string> directs = Directory.GetDirectories(path).Where(s => s.Contains("contract") && !s.Contains("Completing")).ToList();

            foreach (var directory in directs)
            {
                List<string> zipFiles = Directory.GetFiles(directory + @"\daily").ToList();
                List<Task> taskList0 = new List<Task>();
                foreach (var zipFile in zipFiles)
                    taskList0.Add(Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            using (ZipArchive archive = ZipFile.OpenRead(zipFile))
                            {
                                Regex rg = new Regex(@"daily_\d+");
                                List<Task> taskList = new List<Task>();
                                foreach (ZipArchiveEntry file in archive.Entries)
                                    using (StreamReader reader = new StreamReader(file.Open()))
                                    {
                                        string data = reader.ReadToEnd();
                                        taskList.Add(Task.Factory.StartNew(() =>
                                        {
                                            string fileName = zipFile + rg.Match(file.Name).Value.Replace("daily", "");
                                            ParseContract(data, fileName);
                                        }));
                                    }
                                Task.WaitAll(taskList.ToArray());
                            }
                        }
                        catch (Exception err)
                        {
                            File.AppendAllText("invalidFiles.txt", zipFile + "\t" + err.Message + Environment.NewLine);
                        }
                    }));
                Task.WaitAll(taskList0.ToArray());
            }

        }

        public static void ParseContract(string data, string fileName)
        {

            string sql;
            if (data != "")
            {
                Regex rg = new Regex(@"[^\\]+$");
                //string fileName = rg.Match(filePath).Value;

                XDocument doc = XDocument.Parse(data);

                XElement docTypeData = doc.Root
                                 .Element(Getns2Name("body"))
                                 .Element(Getns2Name("item"))
                                 .Element(Getns2Name("contractData"));
                if (docTypeData != null)
                {
                    OpenFreeConn(out var a, out var cn);



                    try
                    {

                        WriteCustomerData(ParseCustomer(docTypeData, data), cn, out int custID);
                        SupplierInfo sp = ParseSupplier(docTypeData);
                        string supID = "null";
                        if (sp != null)
                            WriteSupplierData(sp, cn, out supID);
                        ContractInfo ct = ParseContract(docTypeData);

                        WriteContractData(ct, cn, custID, supID);

                        sql = $"insert into gz.file_status (id, id_session, load_status, file_code, guid_document) values ('{fileName}', {SnID}, 1, 3, '{ct.GUID}')";
                        NpgsqlCommand cmd = new NpgsqlCommand(sql, cn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();

                    }
                    catch (Exception ex)
                    {
                        sql = $"insert into gz.file_status (id, id_session, load_status, error ,file_code) values ('{fileName}','{SnID}', 0, '{ex.Message}', 3)";
                        NpgsqlCommand cmd = new NpgsqlCommand(sql, cn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    CloseBusyConn(a, cn);
                }
            }


        }

        public static void ParseContractXML(string filePath)
        {
            string file = File.ReadAllText(filePath);
            string sql;
            if (file != "")
            {
                Regex rg = new Regex(@"[^\\]+$");
                //string fileName = rg.Match(filePath).Value;
                string fileName = filePath;
                XDocument doc = XDocument.Parse(file);

                XElement docTypeData = doc.Root
                                 .Element(Getns2Name("body"))
                                 .Element(Getns2Name("item"))
                                 .Element(Getns2Name("contractData"));
                if (docTypeData != null)
                {
                    int connNum = ConnectionController.GetFreeConnectionNum();
                    NpgsqlConnection conn = ConnectionController.ConnList.Where(s => s.Num == connNum).FirstOrDefault().Conn;
                    conn.Open();

                    ContractInfo contract = new ContractInfo();



                    try
                    {

                        WriteCustomerData(ParseCustomer(docTypeData, file), conn, out int custID);
                        WriteSupplierData(ParseSupplier(docTypeData), conn, out string supID);
                        ContractInfo ct = ParseContract(docTypeData);

                        WriteContractData(ct, conn, custID, supID);

                        sql = $"insert into file_status (id, id_session, load_status, file_code, guid_document) values ('{fileName}', {SnID}, 1, 3, '{ct.GUID}')";
                        NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();

                    }
                    catch (Exception ex)
                    {
                        sql = $"insert into file_status (id, id_session, load_status, error ,file_code) values ('{fileName}','{SnID}', 0, '{ex.Message}', 3)";
                        NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    conn.Close();
                    ConnectionController.ConnList.Where(s => s.Num == connNum).FirstOrDefault().IsFree = true;

                }
            }


        }

        static CustomerInfo ParseCustomer(XElement element, string file)
        {
            CustomerInfo customer = new CustomerInfo();
            XElement data = element.Element(Getns2Name("customer")).Element(GetGlobalName("mainInfo"));

            customer.Name = data.Element(GetGlobalName("fullName")).Value.Replace("'", "");
            customer.ShortName = data.Element(GetGlobalName("shortName"))?.Value?.Replace("'", "");
            customer.INN = data.Element(GetGlobalName("inn"))?.Value;
            customer.KPP = data.Element(GetGlobalName("kpp"))?.Value;
            customer.OGRN = data.Element(GetGlobalName("ogrn"))?.Value;
            customer.OKATO = data.Element(GetGlobalName("okato"))?.Value;
            customer.EMail = data.Element(GetGlobalName("email"))?.Value;
            customer.OKPO = data.Element(GetGlobalName("okpo"))?.Value;

            string legalAddress = data.Element(GetGlobalName("legalAddress"))?.Value?.Replace("'", "");
            string postalAddress = data.Element(GetGlobalName("postalAddress"))?.Value?.Replace("'", "");
            if (legalAddress != null)
                customer.Address = legalAddress;
            else
                customer.Address = postalAddress;

            return customer;
        }

        static void WriteCustomerData(CustomerInfo customer, NpgsqlConnection conn, out int id)
        {
            string sql = $"select id from gz.customer where inn = '{customer.INN}' and kpp = '{customer.KPP}' and ogrn = '{customer.OGRN}';";
            string ddl = $"insert into gz.customer (naimen, short_naimen, address, inn, kpp, ogrn, okato, okpo, email, id_session)" +
                $"values ('{customer.Name}','{customer.ShortName}','{customer.Address}','{customer.INN}','{customer.KPP}','{customer.OGRN}'," +
                $"'{customer.OKATO}', '{customer.OKPO}', '{customer.EMail}', '{SnID}') on conflict (inn,kpp,ogrn) do nothing;" +
                $" {sql}";

            NpgsqlCommand cmdDdl = new NpgsqlCommand(ddl, conn);
            NpgsqlCommand cmdSql = new NpgsqlCommand(sql, conn);

            id = (int)cmdDdl.ExecuteScalar();
            cmdDdl.Dispose();
        }





        public static ContractInfo ParseContract(XElement data)
        {
            ContractInfo ct = new ContractInfo();
            LotInfo lot = new LotInfo();
            PurchaseInfo prch = new PurchaseInfo();
            ProtocolInfo prot = new ProtocolInfo();
            List<ContractPositionInfo> posList = new List<ContractPositionInfo>();

            ct.GUID = data.Element(Getns2Name("guid")).Value;

            string lotNum = data.Element(Getns2Name("lotNum"))?.Value;
            if (lotNum != null)
                lot.Number = Convert.ToInt32(lotNum);

            prch.GUID = data?.Element(Getns2Name("purchaseNoticeInfo"))?.Element(Getns2Name("guid"))?.Value;
            prch.Number = data?.Element(Getns2Name("purchaseNoticeInfo"))?.Element(Getns2Name("purchaseNoticeNumber"))?.Value;

            ct.PubDate = DateTime.Parse(data.Element(Getns2Name("publicationDate")).Value);
            ct.DocStatus = data.Element(Getns2Name("status")).Value;
            ct.Version = Convert.ToInt32(data.Element(Getns2Name("version")).Value);
            ct.ModDescr = data.Element(Getns2Name("modificationDescription"))?.Value;
            ct.ChangeContract = data.Element(Getns2Name("changeContract"))?.Value;
            ct.Date = DateTime.Parse(data.Element(Getns2Name("contractDate"))?.Value ?? "01/01/1900");
            ct.RegNum = data.Element(Getns2Name("contractRegNumber")).Value;
            lot.GUID = data.Element(Getns2Name("lotGuid"))?.Value;
            ct.Subject = data.Element(Getns2Name("subjectContract"))?.Value.Replace("'", "\"");

            //ct.ResumDate = DateTime.Parse(data.Element(Getns2Name("resumeDate"))?.Value);
            ct.HasSubcontractor = data.Element(Getns2Name("hasSubcontractor"))?.Value;
            ct.Price = data.Element(Getns2Name("price")).Value;

            string date = data.Element(Getns2Name("startExecutionDate"))?.Value;
            if (date == null)
                ct.StartDate = ct.Date;
            else
                ct.StartDate = DateTime.Parse(data.Element(Getns2Name("startExecutionDate")).Value);
            date = data.Element(Getns2Name("endExecutionDate"))?.Value;
            if (date != null)
                ct.EndDate = DateTime.Parse(data.Element(Getns2Name("endExecutionDate")).Value);

            ct.Lot = lot;



            var positions = data.Element(Getns2Name("contractPositions")).Elements(Getns2Name("contractPosition"));
            foreach (var pn in positions)
            {
                ContractPositionInfo pos = new ContractPositionInfo();

                pos.GUID = pn.Element(Getns2Name("guid"))?.Value;

                string ordinalNumber = pn.Element(GetGlobalName("ordinalNumber"))?.Value;
                if (ordinalNumber != null)
                    pos.Number = Convert.ToInt32(ordinalNumber);
                pos.GUID = pn.Element(Getns2Name("guid"))?.Value;
                pos.OKDP = pn?.Element(Getns2Name("okdp"))?.Element(GetGlobalName("code"))?.Value;
                pos.OKPD = pn?.Element(Getns2Name("okpd"))?.Element(GetGlobalName("code"))?.Value;
                pos.OKPD2 = pn?.Element(Getns2Name("okpd2"))?.Element(GetGlobalName("code"))?.Value;
                pos.OKEI = pn?.Element(Getns2Name("okei"))?.Element(GetGlobalName("code"))?.Value;
                pos.OKEIName = pn?.Element(Getns2Name("okei"))?.Element(GetGlobalName("name"))?.Value;
                string qnty = pn?.Element(Getns2Name("qty"))?.Value ?? "-1";
                if (qnty != "-1")
                    pos.Qnty = qnty;

                posList.Add(pos);
            }
            ct.Purchase = prch;
            ct.Protocol = prot;
            ct.Positions = posList;

            return ct;
        }

        static void WriteContractData(ContractInfo ct, NpgsqlConnection cn, int custID, string supID)
        {
            string sql = $"insert into gz.contract (guid, reg_num, version, id_customer, id_supplier, price, date_start, date_end, id_session, guid_purchase, name, id_region) " +
                $"values('{ct.GUID}', '{ct.RegNum}', {ct.Version}, {custID}, {supID}, {ct.Price}, to_date('{ct.StartDate}', 'DD.MM.YYYY'), " +
                $"to_date('{ct.EndDate}', 'DD.MM.YYYY'), {SnID}, '{ct.Purchase.GUID}', '{ct.Subject}', {RegionID})" +
                $"on conflict (reg_num, version) do nothing;";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, cn);
            int cnt = cmd.ExecuteNonQuery();
            cmd.Dispose();

            if (cnt == 1)
            {
                foreach (var pn in ct.Positions)
                {
                    string sqlPn = $"insert into gz.contract_position (okdp, okpd2, okei, quantaty, guid, id_session, guid_contract)" +
                        $"values ('{pn.OKPD}','{pn.OKPD2}','{pn.OKEI}', {ReturnValue(pn.Qnty)}, '{pn.GUID}', {SnID}, '{ct.GUID}')";
                    NpgsqlCommand cmdPn = new NpgsqlCommand(sqlPn, cn);
                    cmdPn.ExecuteNonQuery();
                    cmdPn.Dispose();
                }

                sql = $"insert into gz.lot_contract (guid_lot, id_session, guid_contract, number_lot) " +
                    $"values ({RV.RSt(ct.Lot.GUID)}, {SnID}, '{ct.GUID}', {RV.RInt(ct.Lot.Number)});";
                cmd = new NpgsqlCommand(sql, cn);
                cmd.ExecuteNonQuery();
            }
        }

        static SupplierInfo ParseSupplier(XElement element)
        {
            SupplierInfo sup = new SupplierInfo();

            XElement data = element.Element(Getns2Name("supplierInfo"));

            if (data != null)
            {
                sup.Name = data.Element(Getns2Name("name"))?.Value?.Replace("'", "");
                sup.ShortName = data.Element(Getns2Name("shortName"))?.Value?.Replace("'", "");
                sup.INN = data.Element(Getns2Name("inn"))?.Value;
                sup.KPP = data.Element(Getns2Name("kpp"))?.Value;
                sup.OGRN = data.Element(Getns2Name("ogrn"))?.Value;
                sup.OKATO = data.Element(Getns2Name("okato"))?.Value;
                sup.OKPOF = data.Element(Getns2Name("okpo"))?.Value;
                sup.NonResident = data.Element(Getns2Name("nonResident"))?.Value;
                sup.Code = data.Element(Getns2Name("code"))?.Value;

                string regDate = data.Element(Getns2Name("registrationDate"))?.Value;
                if (regDate != null)
                    sup.RegDate = DateTime.Parse(regDate);
                return sup;
            }
            else
                return null;


        }

        static void WriteSupplierData(SupplierInfo sup, NpgsqlConnection conn, out string idSup)
        {

            string sql = $"insert into gz.supplier (naimen, inn, kpp, ogrn, id_session, reg_date, non_resident, short_name, code, nr_info) " +
            $"values ('{sup.Name}', '{sup.INN}', '{sup.KPP}', '{sup.OGRN}', {SnID}, to_date('{sup.RegDate}', 'DD:MM:YYYY'), {sup.NonResident},'{sup.ShortName}', '{sup.Code}', '{sup.NRInfo}') " +
            $"on conflict (inn, kpp, non_resident, code, nr_info) do nothing; " +
            $"select id from supplier where inn = '{sup.INN}' and kpp = '{sup.KPP}' and non_resident is {sup.NonResident} and code = '{sup.Code}' and nr_info = '{sup.NRInfo}';";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            idSup = cmd.ExecuteScalar().ToString();
            cmd.Dispose();


        }


        static string ReturnValue(string test)
        {
            if (test != null)
                return test;
            else
                return "null";
        }

        static string ReturnString(string test)
        {
            if (test != null)
                return $"'{test}'";
            else
                return "null";
        }
    }
}
