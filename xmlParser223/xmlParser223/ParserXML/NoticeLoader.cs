﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using xmlParser223.DocumentsInfo;
using System.IO.Compression;
using static xmlParser223.RV;


namespace xmlParser223.ParserXML
{
    public class NoticeLoader
    {
        static int SnID { get; set; }
        static int RegionID { get; set; }
        static object SyncLock { get; } = new object();
        static XName GetGlobalName(string prop) => XName.Get(prop, "http://zakupki.gov.ru/223fz/types/1");
        static XName Getns2Name(string prop) => XName.Get(prop, "http://zakupki.gov.ru/223fz/purchase/1");




        public static void ParseNotices(string path, NpgsqlConnection conn, int sID, int regionId)
        {
            SnID = sID;
            RegionID = regionId;



            List<string> directs = Directory.GetDirectories(path).Where(s => s.Contains("Notice")).ToList();

            foreach (var directory in directs)
            {
                List<string> xmlFiles = Directory.GetFiles(directory).ToList();
                List<Task> taskList = new List<Task>();
                foreach (var file in xmlFiles)
                    taskList.Add(Task.Factory.StartNew(() =>
                    {
                        string fileData = File.ReadAllText(file);
                        ParseNotice(fileData, file);
                    }));
                //ParseNotice(file, conn);
                Task.WaitAll(taskList.ToArray());
                //while (taskList.Count > 0)
                //{
                //    int index = Task.WaitAny(taskList.ToArray());

                //    taskList.RemoveAt(index);
                //}

            }


        }

        public static void ParseZipNotices(string path, int sID, int regionId)
        {
            SnID = sID;
            RegionID = regionId;



            List<string> directs = Directory.GetDirectories(path).Where(s => s.Contains("Notice")).ToList();

            foreach (var directory in directs)
            {
                List<string> zipFiles = Directory.GetFiles(directory + @"\daily").ToList();

                List<Task> taskList0 = new List<Task>();
                foreach (var zipFile in zipFiles)
                    taskList0.Add(Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            using (ZipArchive archive = ZipFile.OpenRead(zipFile))
                            {
                                Regex rg = new Regex(@"daily_\d+");
                                List<Task> taskList = new List<Task>();
                                foreach (ZipArchiveEntry file in archive.Entries)
                                    using (StreamReader reader = new StreamReader(file.Open()))
                                    {
                                        string data = reader.ReadToEnd();
                                        taskList.Add(Task.Factory.StartNew(() =>
                                        {
                                            string fileName = zipFile + rg.Match(file.Name).Value.Replace("daily", "");
                                            ParseNotice(data, fileName);
                                        }));
                                    }
                                Task.WaitAll(taskList.ToArray());
                            }
                        }
                        catch (Exception err)
                        {
                            File.AppendAllText("invalidFiles.txt", zipFile + "\t" + err.Message + Environment.NewLine);
                        }
                        
                    }));
                Task.WaitAll(taskList0.ToArray());


            }
        }

        public static void ParseNotice(string fileData, string fileName)
        {

            //string fileData = File.ReadAllText(filePath);


            if (fileData != "")
            {
                //Regex rg = new Regex(@"[^\\]+$");
                //string fileName = rg.Match(filePath).Value;

                NpgsqlConnection conn = new NpgsqlConnection("Server=CBS-PG;Port=5432;User Id=gz_user;Password=gz_user;Database=postgres;");
                conn.Open();

                try
                {
                    XDocument doc = XDocument.Parse(fileData);

                    string docType = doc.Root.Name.LocalName;
                    XElement docTypeData = doc.Root
                                     .Element(Getns2Name("body"))
                                     .Element(Getns2Name("item"))
                                     .Element(Getns2Name($"{docType}Data"));

                    PurchaseInfo prch = ParsePurchase(docTypeData, docType);

                    WriteCustomerData(ParseCustomer(docTypeData), conn, out int custID);
                    WritePurchaseData(prch, conn, custID, out int purchID);
                    if (purchID != 0)
                    {
                        WriteLotData(ParseLot(docTypeData), conn, purchID);

                        string sql = $"insert into gz.file_status (id, id_session, load_status, purchase_num, version, file_code, guid_document) values ('{fileName}', {SnID}, 1, '{prch.Number}', {prch.Version}, 1, '{prch.GUID}')";

                        NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
                catch (Exception ex)
                {

                    string sql = $"insert into gz.file_status (id, id_session, load_status, error, file_code) values ('{fileName}', {SnID}, 0, '{ex.Message}', 1)";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                conn.Close();
            }
        }

        static CustomerInfo ParseCustomer(XElement element)
        {
            CustomerInfo customer = new CustomerInfo();
            XElement data = element.Element(Getns2Name("customer")).Element(GetGlobalName("mainInfo"));

            customer.Name = data.Element(GetGlobalName("fullName")).Value;
            customer.ShortName = data.Element(GetGlobalName("shortName"))?.Value;
            customer.INN = data.Element(GetGlobalName("inn")).Value;
            customer.KPP = data.Element(GetGlobalName("kpp")).Value;
            customer.OGRN = data.Element(GetGlobalName("ogrn")).Value;
            customer.OKATO = data.Element(GetGlobalName("okato"))?.Value;
            customer.EMail = data.Element(GetGlobalName("email"))?.Value;
            customer.OKPO = data.Element(GetGlobalName("okpo"))?.Value;

            string legalAddress = data.Element(GetGlobalName("legalAddress"))?.Value;
            string postalAddress = data.Element(GetGlobalName("postalAddress"))?.Value;
            if (legalAddress != null)
                customer.Address = legalAddress;
            else
                customer.Address = postalAddress;

            return customer;
        }

        static PurchaseInfo ParsePurchase(XElement data, string docType)
        {
            PurchaseInfo prch = new PurchaseInfo();


            prch.Number = data.Element(Getns2Name("registrationNumber")).Value;
            prch.Name = data.Element(Getns2Name("name")).Value.Replace("'", "");
            prch.GUID = data.Element(Getns2Name("guid")).Value;
            prch.Type = docType;
            prch.ChoiceWay = data.Element(Getns2Name("purchaseCodeName")).Value;
            prch.ChoiseWayCode = data.Element(Getns2Name("purchaseMethodCode")).Value;
            prch.Version = Convert.ToInt16(data.Element(Getns2Name("version")).Value);

            string dateBirn = data.Element(Getns2Name("createDateTime")).Value;

            if (prch.Version > 1)
                prch.RefreshDate = DateTime.Parse(data?.Element(Getns2Name("modificationDate"))?.Value ?? dateBirn);
            else
                prch.BirnDate = DateTime.Parse(dateBirn);


            return prch;
        }
        static List<LotInfo> ParseLot(XElement data)
        {
            List<LotInfo> lots = new List<LotInfo>();

            var lotElements = data.Element(Getns2Name("lots")).Elements(GetGlobalName("lot"));
            foreach (var lotEl in lotElements)
            {
                LotInfo lot = new LotInfo();
                var lotData = lotEl.Element(GetGlobalName("lotData"));
                var jointLotData = lotEl.Element(GetGlobalName("jointLotData"));

                IEnumerable<XElement> itemElements;
                XElement lotItemsEl = null;

                string nmc;

                lot.Number = Convert.ToInt32(lotEl.Element(GetGlobalName("ordinalNumber")).Value);
                lot.GUID = lotEl.Element(GetGlobalName("guid"))?.Value;
                if (lotData != null)
                {
                    lot.Name = lotData.Element(GetGlobalName("subject")).Value.Replace("'", "");
                    nmc = lotData.Element(GetGlobalName("initialSum"))?.Value ?? "-1";
                    if (nmc != "-1")
                        lot.NMC = nmc;
                    lot.NMCInfo = lotData.Element(GetGlobalName("initialSumInfo"))?.Value;

                    lotItemsEl = lotData.Element(GetGlobalName("lotItems"));
                }
                else
                {
                    lot.Name = lotEl.Element(GetGlobalName("subject")).Value.Replace("'", "");
                    nmc = nmc = lotEl.Element(GetGlobalName("initialSum"))?.Value ?? "-1";
                    if (nmc != "-1")
                        lot.NMC = nmc;
                    lotItemsEl = lotEl.Element(GetGlobalName("lotItems"));
                    lot.NMCInfo = lotEl.Element(GetGlobalName("initialSumInfo"))?.Value;
                }




                if (lotItemsEl != null)
                    itemElements = lotItemsEl.Elements(GetGlobalName("lotItem"));
                else
                    itemElements = lotEl.Element(GetGlobalName("jointLotData"))
                                          .Element(GetGlobalName("lotCustomers"))
                                          .Element(GetGlobalName("lotCustomer"))
                                          .Element(GetGlobalName("lotCustomerData"))
                                          .Element(GetGlobalName("lotItems"))
                                          .Elements(GetGlobalName("lotItem"));

                List<LotItemInfo> lotItems = new List<LotItemInfo>();
                foreach (var item in itemElements)
                {
                    LotItemInfo lotItem = new LotItemInfo();

                    lotItem.Number = Convert.ToInt32(item.Element(GetGlobalName("ordinalNumber")).Value);
                    lotItem.GUID = item.Element(GetGlobalName("guid"))?.Value;
                    lotItem.OKDP = item?.Element(GetGlobalName("okdp"))?.Element(GetGlobalName("code"))?.Value;
                    lotItem.OKPD = item?.Element(GetGlobalName("okpd"))?.Element(GetGlobalName("code"))?.Value;
                    lotItem.OKPD2 = item?.Element(GetGlobalName("okpd2"))?.Element(GetGlobalName("code"))?.Value;
                    lotItem.OKVED = item?.Element(GetGlobalName("okved"))?.Element(GetGlobalName("code"))?.Value;
                    lotItem.OKVED2 = item?.Element(GetGlobalName("okved2"))?.Element(GetGlobalName("code"))?.Value;
                    lotItem.OKEI = item?.Element(GetGlobalName("okei"))?.Element(GetGlobalName("code"))?.Value;
                    lotItem.OKEIName = item?.Element(GetGlobalName("okei"))?.Element(GetGlobalName("name"))?.Value;
                    string qnty = item?.Element(GetGlobalName("qty"))?.Value ?? "-1";
                    if (qnty != "-1")
                        lotItem.Qnty = qnty;

                    lotItems.Add(lotItem);
                }
                lot.LotItems = lotItems;


                lots.Add(lot);
            }



            return lots;
        }



        static void WriteCustomerData(CustomerInfo customer, NpgsqlConnection conn, out int id)
        {
            string sql = $"select id from gz.customer where inn = '{customer.INN}' and kpp = '{customer.KPP}' and ogrn = '{customer.OGRN}';";
            string ddl = $"insert into gz.customer (naimen, short_naimen, address, inn, kpp, ogrn, okato, okpo, email, id_session)" +
                $"values ({RSt(customer.Name)}, {RSt(customer.ShortName)}, {RSt(customer.Address)},'{customer.INN}','{customer.KPP}','{customer.OGRN}'," +
                $"'{customer.OKATO}', '{customer.OKPO}', '{customer.EMail}', '{SnID}') on conflict do nothing;" +
                $" {sql}";

            NpgsqlCommand cmdDdl = new NpgsqlCommand(ddl, conn);
            NpgsqlCommand cmdSql = new NpgsqlCommand(sql, conn);

            id = (int)cmdDdl.ExecuteScalar();
            cmdDdl.Dispose();
        }

        static void WritePurchaseData(PurchaseInfo prch, NpgsqlConnection conn, int custID, out int id)
        {
            id = 0;

            string sql = $"insert into gz.purchase (reg_num, naimen, determine_way, date_birn, date_refresh, purchase_type, id_region, id_customer, id_session, guid, determine_wayname, version)" +
                $"values ('{prch.Number}', {RSt(prch.Name)}, '{prch.ChoiseWayCode}', to_date('{prch.BirnDate}', 'DD:MM:YYYY')," +
                $"to_date('{prch.RefreshDate}', 'DD:MM:YYYY'), '{prch.Type}', {RegionID},{custID}, {SnID}, '{prch.GUID}', '{prch.ChoiceWay}', {prch.Version}) " +
                $"on conflict (reg_num, version) do nothing returning id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);



            object testId = cmd.ExecuteScalar();
            if (testId != null)
                id = (int)testId;
            cmd.Dispose();


        }

        static void WriteLotData(List<LotInfo> lots, NpgsqlConnection conn, int prchID)
        {
            foreach (var lot in lots)
            {
                string sql = $"insert into gz.lot (id_purchase, number, guid, naimen, start_price, id_session, start_price_info) " +
                    $"values ({prchID}, {lot.Number}, '{lot.GUID}', '{lot.Name}', {ReturnValue(lot.NMC)}, {SnID}, {ReturnString(lot.NMCInfo)}) " +
                    $"on conflict (id_purchase, number) do nothing returning id;";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                object testId = cmd.ExecuteScalar();
                cmd.Dispose();

                if (testId != null)
                {
                    int idLot = (int)testId;

                    foreach (var item in lot.LotItems)
                    {
                        string sqlItem = $"insert into gz.lot_item (number, okdp, okpd2, okved, okved2, okei, quantaty, guid, id_session, id_lot)" +
                            $"values ({item.Number}, '{item.OKDP}', '{item.OKPD2}', " +
                            $"'{item.OKVED}', '{item.OKVED2}', '{item.OKEI}', {ReturnValue(item.Qnty)}, '{item.GUID}', '{SnID}', {idLot});";
                        NpgsqlCommand cmdItem = new NpgsqlCommand(sqlItem, conn);
                        cmdItem.ExecuteNonQuery();
                        cmdItem.Dispose();
                    }
                }
                
            }
        }

        static string ReturnValue(string test)
        {
            if (test != null)
                return test;
            else
                return "null";
        }

        static string ReturnString(string test)
        {
            if (test != null)
                return $"'{test}'";
            else
                return "null";
        }
    }
}
