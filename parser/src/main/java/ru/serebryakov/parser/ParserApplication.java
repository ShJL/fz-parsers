package ru.serebryakov.parser;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.serebryakov.parser.controllers.ControllersConfig;
import ru.serebryakov.parser.controllers.MainWindowController;

@SpringBootApplication
public class ParserApplication extends AbstractJavaFxApplication {

    @Autowired
    @Qualifier("mainView")
    private ControllersConfig.View view;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Парсер Гос Закупок");
        primaryStage.getIcons().add(new Image("/static/icon.png"));
        ((MainWindowController) view.getController()).setPrimaryStage(primaryStage);
        primaryStage.setScene(new Scene(view.getView()));
        primaryStage.setResizable(false);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    public static void main(String[] args) {
        launchApp(ParserApplication.class, args);
    }

}

