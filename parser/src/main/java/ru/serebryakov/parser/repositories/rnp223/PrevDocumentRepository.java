package ru.serebryakov.parser.repositories.rnp223;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.serebryakov.parser.models.rnp223.PrevDocument;

public interface PrevDocumentRepository extends JpaRepository<PrevDocument, Long> {
}
