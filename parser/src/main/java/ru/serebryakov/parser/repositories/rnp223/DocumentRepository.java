package ru.serebryakov.parser.repositories.rnp223;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.serebryakov.parser.models.rnp223.Document;

public interface DocumentRepository extends JpaRepository<Document, Long>{
}
