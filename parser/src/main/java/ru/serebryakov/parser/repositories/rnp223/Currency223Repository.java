package ru.serebryakov.parser.repositories.rnp223;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.serebryakov.parser.models.rnp223.Currency223;

public interface Currency223Repository extends JpaRepository<Currency223, Long> {
}
