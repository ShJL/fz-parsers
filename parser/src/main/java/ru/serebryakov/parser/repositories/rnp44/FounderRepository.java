package ru.serebryakov.parser.repositories.rnp44;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.serebryakov.parser.models.rnp44.Founder;

public interface FounderRepository extends JpaRepository<Founder, Long> {
}
