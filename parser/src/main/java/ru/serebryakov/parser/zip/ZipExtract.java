package ru.serebryakov.parser.zip;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.serebryakov.parser.xml.XmlServiceFactory;

import java.io.*;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Component
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class ZipExtract {

    @Getter
    @Setter
    private String initialDownloadDirectory;

    @Autowired
    private XmlServiceFactory xmlServiceFactory;


    /**
     * @param dataType
     * @param timePeriods
     * @param currentDirectory - путь(объект файла), включая папку с именованием типа даннных(RNP44, RNP223 и т.д)
     * @return
     */
    public boolean searchAndReadArchive(String dataType, List<String> timePeriods, File currentDirectory) {

        if (currentDirectory == null) {
//           TODO: log
            return false;
        }

        for (File entry : currentDirectory.listFiles()) {
            if (entry.isDirectory()) {
                searchAndReadArchive(dataType, timePeriods, entry);
            } else if (entry.isFile() && entry.getName().endsWith(".zip")) {
                if (timePeriods.contains("за всё время")) {
                    try {
                        readDataFromArchive(dataType, entry);
                    } catch (IOException e) {
//                      TODO: log
                        e.printStackTrace();
//                        return false;
                    }
                } else {
                    if (timePeriods.contains(entry.getParentFile().getName().trim())) {
                        try {
                            readDataFromArchive(dataType, entry);
                        } catch (IOException e) {
//                            TODO: log
                            e.printStackTrace();
//                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    private void readDataFromArchive(String dataType, File archive) throws IOException {

        ZipFile zipFile = new ZipFile(archive);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        for (ZipEntry entry = entries.nextElement(); entries.hasMoreElements(); ) {

            StringBuilder xmlData = new StringBuilder();

            try (InputStream is = zipFile.getInputStream(entry)) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        xmlData.append(line);
                    }
                }
            }

            xmlServiceFactory.getXmlService(dataType).parseAndSaveXml(xmlData);

        }
    }
}
