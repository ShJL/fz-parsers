package ru.serebryakov.parser.ftp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FtpServiceFactory {

    @Autowired
    @Qualifier("ftp.rnp.44")
    private FtpClientService ftpClientForRNP44;

    @Autowired
    @Qualifier("ftp.rnp.223")
    private FtpClientService ftpClientForRNP223;

    @Autowired
    @Qualifier("ftp.purchase.44")
    private FtpClientService ftpClientForPurchase44;

    @Autowired
    @Qualifier("ftp.purchase.223")
    private FtpClientService ftpClientForPurchase223;

    public FtpClientService getFtpClientService(String ftpDataType) {

        switch (ftpDataType) {
            case "Закупки по 44-ФЗ":
                return ftpClientForPurchase44;
            case "Закупки по 223-ФЗ":
                return ftpClientForPurchase223;
            case "РНП по 44-ФЗ":
                return ftpClientForRNP44;
            case "РНП по 223-ФЗ":
                return ftpClientForRNP223;
            default:
                return null;
        }

    }
}
