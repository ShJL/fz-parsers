package ru.serebryakov.parser.ftp.purchases;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.serebryakov.parser.ftp.FtpClientService;

import java.io.IOException;
import java.util.List;

@Slf4j
@Component(value = "ftp.purchase.44")
public class FtpClientForPurchase44 implements FtpClientService{

    @Value("${ftp.purchase.44.workingDirectory}")
    private String initialWorkingDirectoryForFtpPurchase44;

    @Value("${directory.name.purchase.44}")
    private String directoryNameForPurchase44;

    @Getter
    @Value("${ftp.purchase.44.host}")
    private String host;

    @Value("${ftp.purchase.44.user}")
    private String username;

    @Value("${ftp.purchase.44.password}")
    private String password;

    public boolean searchAndDownloadFile(String workingDirectory, List<String> timePeriods, String downloadDirectory){

        if (workingDirectory == null){
            workingDirectory = initialWorkingDirectoryForFtpPurchase44;
        }

        return true;
    }

    private void downloadFile(FTPClient ftpClient, String fileName, String year, String uploadDirectory) throws IOException {

    }

}
