package ru.serebryakov.parser;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class TestXml {

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new File(System.getProperty("user.dir") + "/unfairSupplier_41829-16_41829.xml"));
        NodeList elementLis = doc.getElementsByTagName("oos:name");
        for(int i = 0; i < elementLis.getLength(); i++){
            System.out.println(elementLis.item(i).getTextContent());
        }

        NodeList node = doc.getElementsByTagName("oos:currency");
        NodeList list =  node.item(0).getChildNodes();
        for(int i = 0; i < list.getLength(); i++){
            if(list.item(i).getNodeType() == Node.ELEMENT_NODE){
                System.out.println(list.item(i).getNodeName() + ":");
                System.out.println(list.item(i).getTextContent());
            }

        }


    }


}
