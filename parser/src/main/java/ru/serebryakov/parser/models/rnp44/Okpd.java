package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Okpd {

    private String okpdCode;
    private String okpdName;

}
