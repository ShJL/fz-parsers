package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "participation", schema = "purchase223")
public class Participation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "participation_id")
    private Long id;

    @Column(name = "suggestion", columnDefinition = "text")
    private String suggestion;

    @Column(name = "access", columnDefinition = "text")
    private String access;

    @Column(name = "prevent_reason", columnDefinition = "text")
    private String preventReason;

    @Column(name = "lot_num", columnDefinition = "text")
    private String lotNum;

    @Column(name = "guid_lot", columnDefinition = "text")
    private String guidLot;

    @Column(name = "guid_protocol", columnDefinition = "text")
    private String guidProtocol;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "protocol_id")
    private Protocol protocol;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "lot_id")
    private Lot lot;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "supplier_id")
    private Supplier supplier;
}
