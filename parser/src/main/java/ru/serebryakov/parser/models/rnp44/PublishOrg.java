package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "publish_org", schema = "rnp44")
public class PublishOrg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "publish_org_id")
    private Long id;

    @Column(name = "reg_num", columnDefinition = "text")
    private String regNum;

    @Column(name = "full_name", columnDefinition = "text")
    private String fullName;

    @OneToMany(mappedBy = "publishOrg", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<UnfairSupplier> unfairSuppliers;

}
