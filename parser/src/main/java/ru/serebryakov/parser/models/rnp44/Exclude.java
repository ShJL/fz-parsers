package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "exclude", schema = "rnp44")
public class Exclude {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private Long id;

    @Column(name = "exclude_date", columnDefinition = "text")
    private String excludeDate;

    @Column(name = "name", columnDefinition = "text")
    private String name;

    @Column(name = "date", columnDefinition = "text")
    private String date;

    @Column(name = "number", columnDefinition = "text")
    private String number;

    @Column(name = "type", columnDefinition = "text")
    private String type;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "unfair_supplier_id")
    private UnfairSupplier unfairSupplier;



}
