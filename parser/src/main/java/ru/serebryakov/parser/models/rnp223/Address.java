package ru.serebryakov.parser.models.rnp223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "address", schema = "rnp223")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private Long id;

    @Column(name = "area", columnDefinition = "text")
    private String area;

    @Column(name = "corpus", columnDefinition = "text")
    private String corpus;

    @Column(name = "house", columnDefinition = "text")
    private String house;

    @Column(name = "zip", columnDefinition = "text")
    private String zip;

    @Column(name = "region", columnDefinition = "text")
    private String region;

    @Column(name = "structure", columnDefinition = "text")
    private String structure;

    @Column(name = "city", columnDefinition = "text")
    private String city;

    @Column(name = "settlement", columnDefinition = "text")
    private String settlement;

    @Column(name = "street", columnDefinition = "text")
    private String street;

    @Column(name = "email", columnDefinition = "text")
    private String email;

    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Supplier> suppliers;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "country_id")
    private Country country;

}
