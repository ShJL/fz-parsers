package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

/**
 * архив с xml файлами находится в папке  /out/published/{имя_региона}/contractCompleting/daily/
 * ПРОСЬБА: при нахождении xml файла с корневым элементом ns2:contractCancellation (либо похожим на ns2:contractCancellation) отправить данный файл мне
 */
@Data
@Entity
@Table(name = "completing", schema = "purchase223")
public class Completing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "completing_id")
    private Long id;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> guid
     */
    @Column(name = "guid", columnDefinition = "text")
    private String guid;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> ns2:registrationNumber
     */
    @Column(name = "registration_number", columnDefinition = "text")
    private String registrationNumber;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> ns2:createDateTime
     */
    @Column(name = "create_date_time", columnDefinition = "text")
    private String createDateTime;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> ns2:publicationDate
     */
    @Column(name = "publication_date", columnDefinition = "text")
    private String publicationDate;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> guid
     */
    @Column(name = "modification_description", columnDefinition = "text")
    private String modificationDescription;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> ns2:version
     */
    @Column(name = "version", columnDefinition = "text")
    private String version;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> guid -> ns2:completed
     */
    @Column(name = "completed", columnDefinition = "text")
    private String completed;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> ns2:hasPenalty
     */
    @Column(name = "has_penalty", columnDefinition = "text")
    private String hasPenalty;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> ns2:penaltyInfo (при ns2:hasPenalty == TRUE)
     */
    @Column(name = "penalty_info", columnDefinition = "text")
    private String penaltyInfo;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> ns2:contractRegNumber
     */
    @Column(name = "contract_reg_number", columnDefinition = "text")
    private String contractRegNumber;

    /**
     * путь ns2:performanceContract -> ns2:body -> ns2:item -> ns2:contractInfo -> ns2:guid
     * (для связи с сущностью contract)
     */
    @Column(name = "guid_contract", columnDefinition = "text")
    private String guidContract;

    /**
     * путь ns2:contractCancellation -> ns2:body -> ns2:item -> ns2:baseContractCancellationCode
     */
    @Column(name = "base_contract_cancellation_code", columnDefinition = "text")
    private String baseContractCancellationCode;

    /**
     * путь ns2:contractCancellation -> ns2:body -> ns2:item -> ns2:baseContractCancellationName
     */
    @Column(name = "base_contract_cancellation_name", columnDefinition = "text")
    private String baseContractCancellationName;

    /**
     * путь ns2:contractCancellation -> ns2:body -> ns2:item -> ns2:baseRejectionDocCode
     */
    @Column(name = "base_rejection_doc_code", columnDefinition = "text")
    private String baseRejectionDocCode;

    /**
     * путь ns2:contractCancellation -> ns2:body -> ns2:item -> ns2:baseRejectionDocName
     */
    @Column(name = "base_rejection_doc_name", columnDefinition = "text")
    private String baseRejectionDocName;

    /**
     * путь ns2:contractCancellation -> ns2:body -> ns2:item -> ns2:completionDate
     */
    @Column(name = "completion_date", columnDefinition = "text")
    private String completionDate;

    /**
     * путь ns2:contractCancellation -> ns2:body -> ns2:item -> ns2:baseRejectionDocNum
     */
    @Column(name = "base_rejection_doc_num", columnDefinition = "text")
    private String baseRejectionDocNum;

    /**
     * путь ns2:contractCancellation -> ns2:body -> ns2:item -> ns2:baseRejectionDocDate
     */
    @Column(name = "base_rejection_doc_date", columnDefinition = "text")
    private String baseRejectionDocDate;


    /**
     * берётся из имени папки, в которой xml файл находится
     */
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "region_id")
    private Region region;

    @OneToMany(mappedBy = "completing", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Contract> contracts;

}
