package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Cancel {

    private String cancelSignDate;
    private String cancelPerformanceDate;
    private String cancelDate;

}
