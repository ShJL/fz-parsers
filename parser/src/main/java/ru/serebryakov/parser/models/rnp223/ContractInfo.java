package ru.serebryakov.parser.models.rnp223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "contract_info", schema = "rnp223")
public class ContractInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contract_info_id")
    private Long id;

    @Column(name = "name", columnDefinition = "text")
    private String name;

    @Column(name = "sum", columnDefinition = "text")
    private String sum;

    @Column(name = "cancellation_reason", columnDefinition = "text")
    private String cancellationReason;

    @Column(name = "sum_info", columnDefinition = "text")
    private String sumInfo;

    @Column(name = "contract_date", columnDefinition = "text")
    private String contractDate;

    @Column(name = "fulfillment_date", columnDefinition = "text")
    private String fulfillmentDate;

    @Column(name = "cancellation_judgment_date", columnDefinition = "text")
    private String cancellationJudgmentDate;

    @Column(name = "cancellation_date", columnDefinition = "text")
    private String cancellationDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "currency223_id")
    private Currency223 currency223;

    @OneToMany(mappedBy = "contractInfo", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<CommonApplicationInfo> commonApplicationInfos;

    @OneToMany(mappedBy = "contractInfo", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<ContractItem> contractItems;

}
