package ru.serebryakov.parser.models.rnp223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "attachments", schema = "rnp223")
public class Attachments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attachments_id")
    private Long id;

    @Column(name = "totalDocumentsCount", columnDefinition = "text")
    private String totalDocumentsCount;

    @Column(name = "additionalDocumentsCount", columnDefinition = "text")
    private String additionalDocumentsCount;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "dishonest_supplier_data_id")
    private DishonestSupplierData dishonestSupplierData;

    @OneToMany(mappedBy = "attachments", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Document> documents;

    @OneToMany(mappedBy = "attachments", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<PrevDocument> prevDocuments;

}
