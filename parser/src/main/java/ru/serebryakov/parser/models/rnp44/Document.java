package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Document {

    private String documentName;
    private String documentNumber;
    private String documentDate;

}
