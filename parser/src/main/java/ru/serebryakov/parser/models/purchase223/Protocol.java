package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

/**
 * архив с xml файлами находится в папке (в каждом типе purchaseProtocol список lotApplicationsList обрабатывается по-разному):
 * /out/published/{имя_региона}/purchaseProtocol/daily/
 * /out/published/{имя_региона}/purchaseProtocolZK/daily/
 * /out/published/{имя_региона}/purchaseProtocolVK/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAEP/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAAE/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAOA/daily/
 * /out/published/{имя_региона}/purchaseProtocolOSZ/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZOK/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZOA/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZ1AE/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZ2AE/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZAE/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAAE95/daily/
 *
 */
@Data
@Entity
@Table(name = "protocol", schema = "purchase223")
public class Protocol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "protocol_id")
    private Long id;

    /**
     * путь ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95 ->
     * ns2:body -> ns2:item ->
     * ns2:purchaseProtocolData | ns2:purchaseProtocolZKData | ns2:purchaseProtocolVKData | ns2:purchaseProtocolPAEPData | ns2:purchaseProtocolPAAEData | ns2:purchaseProtocolPAOAData |
     * ns2:purchaseProtocolOSZData | ns2:purchaseProtocolRZOKData | ns2:purchaseProtocolRZOAData | ns2:purchaseProtocolRZ1AEData | ns2:purchaseProtocolRZ2AEData | ns2:purchaseProtocolRZAEData | ns2:purchaseProtocolPAAE95Data ->
     * ns2:guid
     */
    @Column(name = "guid", columnDefinition = "text")
    private String guid;

    /**
     * путь ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95 ->
     * ns2:body -> ns2:item ->
     * ns2:purchaseProtocolData | ns2:purchaseProtocolZKData | ns2:purchaseProtocolVKData | ns2:purchaseProtocolPAEPData | ns2:purchaseProtocolPAAEData | ns2:purchaseProtocolPAOAData |
     * ns2:purchaseProtocolOSZData | ns2:purchaseProtocolRZOKData | ns2:purchaseProtocolRZOAData | ns2:purchaseProtocolRZ1AEData | ns2:purchaseProtocolRZ2AEData | ns2:purchaseProtocolRZAEData | ns2:purchaseProtocolPAAE95Data ->
     * ns2:createDateTime
     */
    @Column(name = "create_date_time", columnDefinition = "text")
    private String createDateTime;

    /**
     * путь ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95 ->
     * ns2:body -> ns2:item ->
     * ns2:purchaseProtocolData | ns2:purchaseProtocolZKData | ns2:purchaseProtocolVKData | ns2:purchaseProtocolPAEPData | ns2:purchaseProtocolPAAEData | ns2:purchaseProtocolPAOAData |
     * ns2:purchaseProtocolOSZData | ns2:purchaseProtocolRZOKData | ns2:purchaseProtocolRZOAData | ns2:purchaseProtocolRZ1AEData | ns2:purchaseProtocolRZ2AEData | ns2:purchaseProtocolRZAEData | ns2:purchaseProtocolPAAE95Data ->
     * ns2:registrationNumber
     */
    @Column(name = "registration_number", columnDefinition = "text")
    private String registrationNumber;

    /**
     * путь ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95 ->
     * ns2:body -> ns2:item ->
     * ns2:purchaseProtocolData | ns2:purchaseProtocolZKData | ns2:purchaseProtocolVKData | ns2:purchaseProtocolPAEPData | ns2:purchaseProtocolPAAEData | ns2:purchaseProtocolPAOAData |
     * ns2:purchaseProtocolOSZData | ns2:purchaseProtocolRZOKData | ns2:purchaseProtocolRZOAData | ns2:purchaseProtocolRZ1AEData | ns2:purchaseProtocolRZ2AEData | ns2:purchaseProtocolRZAEData | ns2:purchaseProtocolPAAE95Data ->
     * ns2:version
     */
    @Column(name = "version", columnDefinition = "text")
    private String version;

    /**
     * путь ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95 ->
     * ns2:body -> ns2:item ->
     * ns2:purchaseProtocolData | ns2:purchaseProtocolZKData | ns2:purchaseProtocolVKData | ns2:purchaseProtocolPAEPData | ns2:purchaseProtocolPAAEData | ns2:purchaseProtocolPAOAData |
     * ns2:purchaseProtocolOSZData | ns2:purchaseProtocolRZOKData | ns2:purchaseProtocolRZOAData | ns2:purchaseProtocolRZ1AEData | ns2:purchaseProtocolRZ2AEData | ns2:purchaseProtocolRZAEData | ns2:purchaseProtocolPAAE95Data ->
     * ns2:missedContest
     */
    @Column(name = "missed_contest", columnDefinition = "text")
    private String missedContest;

    /**
     * путь ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95 ->
     * ns2:body -> ns2:item ->
     * ns2:purchaseProtocolData | ns2:purchaseProtocolZKData | ns2:purchaseProtocolVKData | ns2:purchaseProtocolPAEPData | ns2:purchaseProtocolPAAEData | ns2:purchaseProtocolPAOAData |
     * ns2:purchaseProtocolOSZData | ns2:purchaseProtocolRZOKData | ns2:purchaseProtocolRZOAData | ns2:purchaseProtocolRZ1AEData | ns2:purchaseProtocolRZ2AEData | ns2:purchaseProtocolRZAEData | ns2:purchaseProtocolPAAE95Data ->
     * ns2:missedReason
     */
    @Column(name = "missed_reason", columnDefinition = "text")
    private String missedReason;

    /**
     * путь ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95 ->
     * ns2:body -> ns2:item ->
     * ns2:purchaseProtocolData | ns2:purchaseProtocolZKData | ns2:purchaseProtocolVKData | ns2:purchaseProtocolPAEPData | ns2:purchaseProtocolPAAEData | ns2:purchaseProtocolPAOAData |
     * ns2:purchaseProtocolOSZData | ns2:purchaseProtocolRZOKData | ns2:purchaseProtocolRZOAData | ns2:purchaseProtocolRZ1AEData | ns2:purchaseProtocolRZ2AEData | ns2:purchaseProtocolRZAEData | ns2:purchaseProtocolPAAE95Data ->
     * ns2:purchaseInfo -> purchaseNoticeNumber
     */
    @Column(name = "purchase_notice_number", columnDefinition = "text")
    private String purchaseNoticeNumber;

    /**
     * путь ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95 ->
     * ns2:body -> ns2:item ->
     * ns2:purchaseProtocolData | ns2:purchaseProtocolZKData | ns2:purchaseProtocolVKData | ns2:purchaseProtocolPAEPData | ns2:purchaseProtocolPAAEData | ns2:purchaseProtocolPAOAData |
     * ns2:purchaseProtocolOSZData | ns2:purchaseProtocolRZOKData | ns2:purchaseProtocolRZOAData | ns2:purchaseProtocolRZ1AEData | ns2:purchaseProtocolRZ2AEData | ns2:purchaseProtocolRZAEData | ns2:purchaseProtocolPAAE95Data ->
     * ns2:purchaseInfo -> guid
     * !!! при нахождении файла с данным полем файл отправить мне !!!
     */
    @Column(name = "purchase_guid", columnDefinition = "text")
    private String purchaseGuid;

    /**
     * ns2:purchaseProtocol | ns2:purchaseProtocolZK | ns2:purchaseProtocolVK | ns2:purchaseProtocolPAEP | ns2:purchaseProtocolPAAE | ns2:purchaseProtocolPAOA |
     * ns2:purchaseProtocolOSZ | ns2:purchaseProtocolRZOK | ns2:purchaseProtocolRZOA | ns2:purchaseProtocolRZ1AE | ns2:purchaseProtocolRZ2AE | ns2:purchaseProtocolRZAE | ns2:purchaseProtocolPAAE95
     * берётся с имени тега без указания пространства имён
     */
    @Column(name = "type", columnDefinition = "text")
    private String type;


    /**
     * ссылка на {@link Lot}
     */



    @OneToMany(mappedBy = "protocol", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Participation> participations;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "purchase_id")
    private Purchase purchase;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "region_id")
    private Region region;


}
