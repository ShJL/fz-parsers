package ru.serebryakov.parser.models.rnp223;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "contract_item", schema = "rnp223")
public class ContractItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contract_item_id")
    private Long id;

    @Column(name = "name", columnDefinition = "text")
    private String name;

    @Column(name = "okpd2_code", columnDefinition = "text")
    private String okpd2Code;

    @Column(name = "okpd2_name", columnDefinition = "text")
    private String okpd2Name;

    @Column(name = "okved2_code", columnDefinition = "text")
    private String okved2Code;

    @Column(name = "okved2_name", columnDefinition = "text")
    private String okved2Name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "contract_info_id")
    private ContractInfo contractInfo;

}
