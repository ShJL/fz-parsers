package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

/**
 * архив с xml файлами находится в папке (в каждом типе purchaseProtocol список lotApplicationsList обрабатывается по-разному):
 * /out/published/{имя_региона}/purchaseProtocol/daily/
 * /out/published/{имя_региона}/purchaseProtocolZK/daily/
 * /out/published/{имя_региона}/purchaseProtocolVK/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAEP/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAAE/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAOA/daily/
 * /out/published/{имя_региона}/purchaseProtocolOSZ/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZOK/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZOA/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZ1AE/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZ2AE/daily/
 * /out/published/{имя_региона}/purchaseProtocolRZAE/daily/
 * /out/published/{имя_региона}/purchaseProtocolPAAE95/daily/
 * сущность Lot находится по пути: ns2:purchaseProtocol{тип протокола} -> ns2:body -> ns2:item ->
 * ns2:lotApplicationsList
 */
@Data
@Entity
@Table(name = "lot", schema = "purchase223")
public class Lot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lot_id")
    private Long id;


    /**
     * в purchaseProtocol: ... ns2:protocolLotApplications -> ns2:lot -> ns2:ordinalNumber
     * в purchaseProtocolZK: ... ns2:protocolZKLotApplications -> ns2:lot -> ns2:ordinalNumber
     * в purchaseProtocolVK: ... ns2:protocolLotApplications -> ns2:lot -> ns2:ordinalNumber
     * в purchaseProtocolPAEP: ... ns2:protocolLotApplications -> ns2:lot -> ns2:ordinalNumber
     * в purchaseProtocolPAAE: ... ns2:protocolLotApplications -> ns2:lot -> ns2:ordinalNumber
     */
    @Column(name = "ordinal_number", columnDefinition = "text")
    private String ordinalNumber;

    /**
     * в purchaseProtocol: ... ns2:protocolLotApplications -> ns2:lot -> ns2:guid
     * в purchaseProtocolZK: ... ns2:protocolZKLotApplications -> ns2:lot -> ns2:guid
     * в purchaseProtocolVK: ... ns2:protocolLotApplications -> ns2:lot -> ns2:guid
     * в purchaseProtocolPAEP: ... ns2:protocolLotApplications -> ns2:lot -> ns2:guid
     * в purchaseProtocolPAAE: ... ns2:protocolLotApplications -> ns2:lot -> ns2:guid
     */
    @Column(name = "guid", columnDefinition = "text")
    private String guid;


    @OneToMany(mappedBy = "lot", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<LotContract> lotContracts;

    @OneToMany(mappedBy = "lot", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<LotItem> lotItems;

    @OneToMany(mappedBy = "lot", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Participation> participations;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "purchase_id")
    private Purchase purchase;

}
