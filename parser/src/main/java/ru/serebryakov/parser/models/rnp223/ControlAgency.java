package ru.serebryakov.parser.models.rnp223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "control_agency", schema = "rnp223")
public class ControlAgency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "control_agency_id")
    private Long id;

    @Column(name = "full_name", columnDefinition = "text")
    private String fullName;

    @Column(name = "short_name", columnDefinition = "text")
    private String shortName;

    @Column(name = "inn", columnDefinition = "text")
    private String inn;

    @Column(name = "iko", columnDefinition = "text")
    private String iko;

    @Column(name = "kpp", columnDefinition = "text")
    private String kpp;

    @Column(name = "ogrn", columnDefinition = "text")
    private String ogrn;

    @Column(name = "legal_address", columnDefinition = "text")
    private String legalAddress;

    @Column(name = "postal_address", columnDefinition = "text")
    private String postalAddress;

    @Column(name = "phone", columnDefinition = "text")
    private String phone;

    @Column(name = "fax", columnDefinition = "text")
    private String fax;

    @Column(name = "email", columnDefinition = "text")
    private String email;

    @Column(name = "okato", columnDefinition = "text")
    private String okato;

    @Column(name = "okopf", columnDefinition = "text")
    private String okopf;

    @Column(name = "okopf_name", columnDefinition = "text")
    private String okopfName;

    @Column(name = "okpo", columnDefinition = "text")
    private String okpo;

    @Column(name = "customer_registration_date")
    private String customerRegistrationDate;

    @OneToMany(mappedBy = "controlAgency", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<CommonApplicationInfo> commonApplicationInfos;

}
