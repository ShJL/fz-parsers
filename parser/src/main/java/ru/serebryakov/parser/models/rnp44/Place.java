package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Place {

    private String placeEmail;
    private String placeZip;
    private String placeInfo;

}
