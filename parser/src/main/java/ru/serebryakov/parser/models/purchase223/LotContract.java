package ru.serebryakov.parser.models.purchase223;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "lot_contract", schema = "purchase223")
public class LotContract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lot_contract_id")
    private Long id;

    @Column(name = "guid_purchase", columnDefinition = "text")
    private String guidPurchase;

    @Column(name = "reg_num_purchase", columnDefinition = "text")
    private String regNumPurchase;

    @Column(name = "number_lot", columnDefinition = "text")
    private String numberLot;

    @Column(name = "guid_lot", columnDefinition = "text")
    private String guidLot;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "contract_id")
    private Contract contract;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "lot_id")
    private Lot lot;

}
