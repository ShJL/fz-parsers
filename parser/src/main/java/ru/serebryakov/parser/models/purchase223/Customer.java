package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "customer", schema = "purchase223")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private Long id;

    @Column(name = "naimen", columnDefinition = "text")
    private String naimen;

    @Column(name = "short_naimen", columnDefinition = "text")
    private String shortNaimen;

    @Column(name = "address", columnDefinition = "text")
    private String address;

    @Column(name = "inn", columnDefinition = "text")
    private String inn;

    @Column(name = "kpp", columnDefinition = "text")
    private String kpp;

    @Column(name = "ogrn", columnDefinition = "text")
    private String ogrn;

    @Column(name = "okato", columnDefinition = "text")
    private String okato;

    @Column(name = "okpo", columnDefinition = "text")
    private String okpo;

    @Column(name = "email", columnDefinition = "text")
    private String email;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Purchase> purchases;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Contract> contracts;


}
