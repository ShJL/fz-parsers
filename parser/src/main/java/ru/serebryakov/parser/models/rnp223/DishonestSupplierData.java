package ru.serebryakov.parser.models.rnp223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "dishonest_supplier_data", schema = "rnp223")
public class DishonestSupplierData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dishonest_supplier_data_id")
    private Long id;

    @Column(name = "registration_number", columnDefinition = "text")
    private String registrationNumber;

    @Column(name = "version", columnDefinition = "text")
    private String version;

    @Column(name = "application_number", columnDefinition = "text")
    private String applicationNumber;

    @Column(name = "include_reason_info", columnDefinition = "text")
    private String includeReasonInfo;

    @Column(name = "status", columnDefinition = "text")
    private String status;

    @Column(name = "modification_description", columnDefinition = "text")
    private String modificationDescription;

    @Column(name = "url_EIS", columnDefinition = "text")
    private String urlEIS;

    @Column(name = "url_VSRZ", columnDefinition = "text")
    private String urlVSRZ;

    @Column(name = "approve_status", columnDefinition = "text")
    private String approveStatus;

    @Column(name = "publication_date_time", columnDefinition = "text")
    private String publicationDateTime;

    @Column(name = "create_date_time", columnDefinition = "text")
    private String createDateTime;

    @Column(name = "include_date", columnDefinition = "text")
    private String includeDate;

    @Column(name = "exclude_date", columnDefinition = "text")
    private String excludeDate;

    @OneToMany(mappedBy = "dishonestSupplierData", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Attachments> attachments;

    @OneToMany(mappedBy = "dishonestSupplierData", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<CommonApplicationInfo> commonApplicationInfos;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "placer_id")
    private Placer placer;


}
