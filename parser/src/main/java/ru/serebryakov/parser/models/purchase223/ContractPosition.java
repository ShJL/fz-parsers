package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "contract_position", schema = "purchase223")
public class ContractPosition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contract_position_id")
    private Long id;

    @Column(name = "guid", columnDefinition = "text")
    private String guid;

    @Column(name = "okdp", columnDefinition = "text")
    private String okdp;

    @Column(name = "okei", columnDefinition = "text")
    private String okei;

    @Column(name = "quantaty", columnDefinition = "text")
    private String quantaty;

    @Column(name = "okpd", columnDefinition = "text")
    private String okpd;

    @Column(name = "okpd2", columnDefinition = "text")
    private String okpd2;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "contract_id")
    private Contract contract;

}
