package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "contract", schema = "rnp44")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contract_id")
    private Long id;

    @Column(name = "sign_date", columnDefinition = "text")
    private String signDate;

    @Column(name = "reg_num", columnDefinition = "text")
    private String regNum;

    @Column(name = "product_info", columnDefinition = "text")
    private String productInfo;

    @Column(name = "price", columnDefinition = "text")
    private String price;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "baseName", column = @Column(name = "base_name")),
            @AttributeOverride(name = "baseDate", column = @Column(name = "base_date")),
            @AttributeOverride(name = "baseNumber", column = @Column(name = "base_number"))
    })
    private Base base;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "okpdCode", column = @Column(name = "okpd_code")),
            @AttributeOverride(name = "okpdName", column = @Column(name = "okpd_name"))
    })
    private Okpd okpd;


    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "cancelSignDate", column = @Column(name = "cancel_sign_date")),
            @AttributeOverride(name = "cancelPerformanceDate", column = @Column(name = "cancel_performance_date")),
            @AttributeOverride(name = "cancelDate", column = @Column(name = "cancel_date"))
    })
    private Cancel cancel;


    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "currency_id")
    private Currency currency;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "unfair_supplier_id")
    private UnfairSupplier unfairSupplier;

}
