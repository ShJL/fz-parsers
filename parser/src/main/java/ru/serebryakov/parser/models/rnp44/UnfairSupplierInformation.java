package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "unfair_supplier_information", schema = "rnp44")
public class UnfairSupplierInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unfair_supplier_information_id")
    private Long id;

    @Column(name = "full_name", columnDefinition = "text")
    private String fullName;

    @Column(name = "type", columnDefinition = "text")
    private String type;

    @Column(name = "firm_name", columnDefinition = "text")
    private String firmName;

    @Column(name = "kpp", columnDefinition = "text")
    private String kpp;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "kladrCode", column = @Column(name = "kladr_code")),
            @AttributeOverride(name = "kladrType", column = @Column(name = "kladr_type")),
            @AttributeOverride(name = "kladrFullName", column = @Column(name = "kladr_full_name")),
            @AttributeOverride(name = "kladrSubjectRF", column = @Column(name = "kladr_subjectRF")),
            @AttributeOverride(name = "kladrArea", column = @Column(name = "kladr_area")),
            @AttributeOverride(name = "kladrCity", column = @Column(name = "kladr_city")),
            @AttributeOverride(name = "kladrStreet", column = @Column(name = "kladr_street")),
            @AttributeOverride(name = "kladrBuilding", column = @Column(name = "kladr_building")),
            @AttributeOverride(name = "kladrOffice", column = @Column(name = "kladr_office"))
    })
    private Kladr kladr;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "countryCode", column = @Column(name = "country_code")),
            @AttributeOverride(name = "countryFullName", column = @Column(name = "country_full_name"))
    })
    private Country country;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "placeEmail", column = @Column(name = "place_email")),
            @AttributeOverride(name = "placeZip", column = @Column(name = "place_zip")),
            @AttributeOverride(name = "placeInfo", column = @Column(name = "place_info"))
    })
    private Place place;

    @OneToMany(mappedBy = "unfairSupplierInformation", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<UnfairSupplier> unfairSuppliers;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(schema = "rnp44", name = "founder_unfair_supplier_information",
            joinColumns = @JoinColumn(name = "unfair_supplier_information_id"),
            inverseJoinColumns = @JoinColumn(name = "founder_id"))
    private Set<Founder> founders;

}
