package ru.serebryakov.parser.models.rnp44;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class Kladr {

    private String kladrCode;
    private String kladrType;
    private String kladrFullName;
    private String kladrSubjectRF;
    private String kladrArea;
    private String kladrCity;
    private String kladrStreet;
    private String kladrBuilding;
    private String kladrOffice;

}
