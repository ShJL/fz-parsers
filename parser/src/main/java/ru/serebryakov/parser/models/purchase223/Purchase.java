package ru.serebryakov.parser.models.purchase223;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "purchase", schema = "purchase223")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "purchase_id")
    private Long id;

    @Column(name = "reg_num", columnDefinition = "text")
    private String regNum;

    @Column(name = "version", columnDefinition = "text")
    private String version;

    @Column(name = "guid", columnDefinition = "text")
    private String guid;

    @Column(name = "naimen", columnDefinition = "text")
    private String naimen;

    @Column(name = "determine_way", columnDefinition = "text")
    private String determineWay;

    @Column(name = "determine_wayname", columnDefinition = "text")
    private String determineWayname;

    @Column(name = "date_birn", columnDefinition = "text")
    private String dateBirn;

    @Column(name = "date_refresh", columnDefinition = "text")
    private String dateRefresh;

    @Column(name = "purchase_type", columnDefinition = "text")
    private String purchaseType;


    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "region_id")
    private Region region;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "purchase", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Protocol> protocols;

    @OneToMany(mappedBy = "purchase", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Lot> lots;

    @OneToMany(mappedBy = "purchase", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Contract> contracts;

}
