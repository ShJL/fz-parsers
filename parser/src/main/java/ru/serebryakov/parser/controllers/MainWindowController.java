package ru.serebryakov.parser.controllers;

import com.jfoenix.controls.JFXButton;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.controlsfx.control.CheckComboBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import ru.serebryakov.parser.ftp.FtpClientService;
import ru.serebryakov.parser.ftp.FtpServiceFactory;
import ru.serebryakov.parser.zip.ZipExtract;

import javax.annotation.PostConstruct;
import java.io.File;
import java.time.LocalDate;
import java.util.List;

@Slf4j
public class MainWindowController {

    @Value("${directory.name.purchase.44}")
    private String directoryNameForPurchase44;

    @Value("${directory.name.purchase.223}")
    private String directoryNameForPurchase223;

    @Value("${directory.name.rnp.44}")
    private String directoryNameForRnp44;

    @Value("${directory.name.rnp.223}")
    private String directoryNameForRnp223;

    @Autowired
    private FtpServiceFactory ftpServiceFactory;

    @Autowired
    private ZipExtract zipExtract;

    @Autowired
    @Qualifier("referenceView")
    private ControllersConfig.View referenceView;

    @Autowired
    @Qualifier("exportView")
    private ControllersConfig.View exportView;

    @Autowired
    @Qualifier("backupView")
    private ControllersConfig.View backupView;

    private Stage referenceStage, exportStage, backupStage;

    @Setter
    @Getter
    private Stage primaryStage;

    @FXML
    private Label directoryLabel, errorLabel;

    @FXML
    private CheckComboBox<String> timePeriods;

    @FXML
    private JFXButton ftpButton, dbButton, directoryButton, exportButton, backupButton;

    @FXML
    private ToggleGroup dataType;

    @FXML
    private ProgressIndicator statusIndicator;

    @FXML
    private Tooltip directoryTooltip;


    /**
     * метод JavaFx
     */
    @FXML
    public void initialize() {

        initTimePeriods();

        statusIndicator.setVisible(false);
        errorLabel.setVisible(false);


    }

    @PostConstruct
    public void initDataFromSpringContext() {

    }

    @FXML
    public void chooseDirectory() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Выберите папку");
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File directory = directoryChooser.showDialog(primaryStage);
        if (directory != null) {
            directoryLabel.setText(directory.getAbsolutePath());
            directoryTooltip.setText(directory.getAbsolutePath());
        }
    }

    @FXML
    void downloadFromFtp() {

        String directoryForFiles = directoryLabel.getText();

        if (directoryForFiles == null || directoryForFiles.trim().equals("")) {
            errorLabel.setText("Укажите папку для загрузки файлов");
            statusIndicator.setVisible(false);
            errorLabel.setVisible(true);
            return;
        } else {
            errorLabel.setVisible(false);
            statusIndicator.setVisible(true);

        }

        exportButton.setDisable(true);
        backupButton.setDisable(true);
        ftpButton.setDisable(true);
        dbButton.setDisable(true);
        directoryButton.setDisable(true);
        timePeriods.setDisable(true);
        dataType.getToggles().forEach(toggle -> ((RadioButton) toggle).setDisable(true));

        errorLabel.setVisible(false);
        statusIndicator.setVisible(true);

        String selectedRButtonName = ((RadioButton) dataType.getSelectedToggle()).getText();
        List<String> periods = timePeriods.getCheckModel().getCheckedItems();

        FtpClientService ftpClientService = ftpServiceFactory.getFtpClientService(selectedRButtonName);

        Service<Boolean> service = new Service<Boolean>() {
            @Override
            protected Task<Boolean> createTask() {
                return new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {

                        boolean result;

                        try {
                            result = ftpClientService.searchAndDownloadFile(null, periods, directoryLabel.getText().trim());
                        } catch (Exception ex) {
                            log.error(getClass().getName() + ": При загрузке файлов с FTP сервера возникла ошибка");
                            result = false;
                        }

                        return result;
                    }
                };
            }
        };

        service.setOnSucceeded(event -> {

            exportButton.setDisable(false);
            backupButton.setDisable(false);
            ftpButton.setDisable(false);
            dbButton.setDisable(false);
            directoryButton.setDisable(false);
            timePeriods.setDisable(false);
            dataType.getToggles().forEach(toggle -> ((RadioButton) toggle).setDisable(false));

            statusIndicator.setVisible(false);

            if (((Boolean) event.getSource().getValue())) {
                errorLabel.setTextFill(Color.GREEN);
                errorLabel.setText("Загрузка файлов с FTP сервера прошла успешно");
            } else {
                errorLabel.setTextFill(Color.RED);
                errorLabel.setText("При загрузке с FTP сервера возникла ошибка");
            }

            errorLabel.setVisible(true);
        });

        service.setOnFailed(event -> {

            exportButton.setDisable(false);
            backupButton.setDisable(false);
            ftpButton.setDisable(false);
            dbButton.setDisable(false);
            directoryButton.setDisable(false);
            timePeriods.setDisable(false);
            dataType.getToggles().forEach(toggle -> ((RadioButton) toggle).setDisable(false));

            statusIndicator.setVisible(false);

            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("При загрузке с FTP сервера возникла ошибка");

            errorLabel.setVisible(true);

        });

        service.start();


    }

    @FXML
    void saveInDb() {

        String directoryForFiles = directoryLabel.getText();

        if (directoryForFiles == null || directoryForFiles.trim().equals("")) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Укажите папку c файлами");
            statusIndicator.setVisible(false);
            errorLabel.setVisible(true);
            return;
        } else {
            errorLabel.setVisible(false);
            statusIndicator.setVisible(true);

        }

        exportButton.setDisable(true);
        backupButton.setDisable(true);
        ftpButton.setDisable(true);
        dbButton.setDisable(true);
        directoryButton.setDisable(true);
        timePeriods.setDisable(true);
        dataType.getToggles().forEach(toggle -> ((RadioButton) toggle).setDisable(true));

        errorLabel.setVisible(false);
        statusIndicator.setVisible(true);

        String selectedRButtonName = ((RadioButton) dataType.getSelectedToggle()).getText().trim();
        List<String> periods = timePeriods.getCheckModel().getCheckedItems();
        StringBuilder directory = new StringBuilder(directoryLabel.getText().trim());
        directory.append(File.separatorChar);

        switch (selectedRButtonName) {
            case "Закупки по 44-ФЗ":
                directory.append(directoryNameForPurchase44);
                break;
            case "Закупки по 223-ФЗ":
                directory.append(directoryNameForPurchase223);
                break;
            case "РНП по 44-ФЗ":
                directory.append(directoryNameForRnp44);
                break;
            case "РНП по 223-ФЗ":
                directory.append(directoryNameForRnp223);
                break;
            default:
        }

        Service<Boolean> service = new Service<Boolean>() {
            @Override
            protected Task<Boolean> createTask() {
                return new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {

                        boolean result;

                        try {
                            result = zipExtract.searchAndReadArchive(selectedRButtonName, periods, new File(directory.toString()));
                        } catch (Exception ex){
                            log.error(getClass().getName() + ": При сохранении в базу данных возникла ошибка");
                            result = false;
                        }

                        return result;

                    }
                };
            }
        };

        service.setOnSucceeded(event -> {
            exportButton.setDisable(false);
            backupButton.setDisable(false);
            ftpButton.setDisable(false);
            dbButton.setDisable(false);
            directoryButton.setDisable(false);
            timePeriods.setDisable(false);
            dataType.getToggles().forEach(toggle -> ((RadioButton) toggle).setDisable(false));

            statusIndicator.setVisible(false);

            if (((Boolean) event.getSource().getValue())) {
                errorLabel.setTextFill(Color.GREEN);
                errorLabel.setText("Сохранение в базу данных прошло успешно");
            } else {
                errorLabel.setTextFill(Color.RED);
                errorLabel.setText("При сохранении в базу данных возникла ошибка");
            }

            errorLabel.setVisible(true);
        });

        service.setOnFailed(event -> {
            exportButton.setDisable(false);
            backupButton.setDisable(false);
            ftpButton.setDisable(false);
            dbButton.setDisable(false);
            directoryButton.setDisable(false);
            timePeriods.setDisable(false);
            dataType.getToggles().forEach(toggle -> ((RadioButton) toggle).setDisable(false));

            statusIndicator.setVisible(false);

            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("При сохранении в базу данных возникла ошибка");

            errorLabel.setVisible(true);
        });

        service.start();

    }

    @FXML
    void openReferenceWindow() {
        if (referenceStage == null) {
            referenceStage = new Stage();
            referenceStage.setResizable(false);
            referenceStage.getIcons().add(new Image("/static/icon.png"));
            referenceStage.setTitle("Справка");
            referenceStage.setScene(new Scene(referenceView.getView()));
        }
        referenceStage.show();
    }

    @FXML
    void openExportWindow() {
        if (exportStage == null) {
            exportStage = new Stage();
            exportStage.setResizable(false);
            exportStage.getIcons().add(new Image("/static/icon.png"));
            exportStage.setTitle("Экспорт таблиц");
            exportStage.setScene(new Scene(exportView.getView()));
        }
        exportStage.show();
    }

    @FXML
    void openBackupWindow() {
        if (backupStage == null) {
            backupStage = new Stage();
            backupStage.setResizable(false);
            backupStage.getIcons().add(new Image("/static/icon.png"));
            backupStage.setTitle("Бэкап");
            backupStage.setScene(new Scene(backupView.getView()));
        }
        backupStage.show();
    }


    private void initTimePeriods() {

        timePeriods.getItems().add("за всё время");
        int currentYear = LocalDate.now().getYear();
        for (int year = 2014; year <= currentYear; year++) {
            timePeriods.getItems().add(String.valueOf(year));
        }

        timePeriods.getCheckModel().check("за всё время");

        timePeriods.getCheckModel().getCheckedItems().addListener((ListChangeListener<String>) change -> {
            while (change.next()) {

                String periodName;

                if (change.wasAdded()) {
                    periodName = change.getAddedSubList().get(0);
                    if (periodName.equals("за всё время")) {
                        for (String checkName : timePeriods.getItems()) {
                            if (!checkName.equals("за всё время")) {
                                timePeriods.getCheckModel().clearCheck(checkName);
                            }
                        }
                    } else {
                        timePeriods.getCheckModel().clearCheck("за всё время");
                    }
                }
            }
        });
    }


}
