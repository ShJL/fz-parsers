package ru.serebryakov.parser.controllers;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ReferenceWindowController {

    @FXML
    private ImageView mainWindowImage, directoryImage, exportWindowImage, backupWindowImage;

    @FXML
    public void initialize() {
        mainWindowImage.setImage(new Image("/static/main.PNG"));
        directoryImage.setImage(new Image("/static/directory.PNG"));
        exportWindowImage.setImage(new Image("/static/export.PNG"));
        backupWindowImage.setImage(new Image("/static/backup.PNG"));

    }

}
