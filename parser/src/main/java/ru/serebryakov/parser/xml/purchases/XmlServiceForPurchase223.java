package ru.serebryakov.parser.xml.purchases;

import org.springframework.stereotype.Component;
import ru.serebryakov.parser.xml.XmlService;

@Component(value = "xml.purchase.223")
public class XmlServiceForPurchase223 implements XmlService {

    @Override
    public boolean parseAndSaveXml(StringBuilder xmlData) {
        System.out.println("PURCHASE223");
        return true;
    }
}
