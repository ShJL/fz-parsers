package ru.serebryakov.parser.xml.purchases;

import org.springframework.stereotype.Component;
import ru.serebryakov.parser.xml.XmlService;

@Component(value = "xml.purchase.44")
public class XmlServiceForPurchase44 implements XmlService {

    @Override
    public boolean parseAndSaveXml(StringBuilder xmlData) {
        System.out.println("PURCHASE44");
        return true;
    }
}
