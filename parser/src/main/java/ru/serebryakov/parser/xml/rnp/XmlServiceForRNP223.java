package ru.serebryakov.parser.xml.rnp;

import org.springframework.stereotype.Component;
import ru.serebryakov.parser.xml.XmlService;

@Component(value = "xml.rnp.223")
public class XmlServiceForRNP223 implements XmlService{

    @Override
    public boolean parseAndSaveXml(StringBuilder xmlData) {
        System.out.println("RNP223");
        return true;
    }

}
