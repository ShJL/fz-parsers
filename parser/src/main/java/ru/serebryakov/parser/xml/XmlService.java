package ru.serebryakov.parser.xml;

public interface XmlService {

    boolean parseAndSaveXml(StringBuilder xmlData);

}
