package ru.serebryakov.parser.xml.rnp;

import org.springframework.stereotype.Component;
import ru.serebryakov.parser.xml.XmlService;

@Component(value = "xml.rnp.44")
public class XmlServiceForRNP44 implements XmlService {

    @Override
    public boolean parseAndSaveXml(StringBuilder xmlData) {
        System.out.println("RNP44");
        return true;
    }
}
