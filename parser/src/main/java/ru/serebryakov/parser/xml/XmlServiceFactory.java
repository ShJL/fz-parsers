package ru.serebryakov.parser.xml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class XmlServiceFactory {

    @Autowired
    @Qualifier("xml.rnp.44")
    private XmlService xmlServiceForRNP44;

    @Autowired
    @Qualifier("xml.rnp.223")
    private XmlService xmlServiceForRNP223;

    @Autowired
    @Qualifier("xml.purchase.44")
    private XmlService xmlServiceForPurchase44;

    @Autowired
    @Qualifier("xml.purchase.223")
    private XmlService xmlServiceForPurchase223;

    public XmlService getXmlService(String dataType){

        switch (dataType) {
            case "Закупки по 44-ФЗ":
                return xmlServiceForPurchase44;
            case "Закупки по 223-ФЗ":
                return xmlServiceForPurchase223;
            case "РНП по 44-ФЗ":
                return xmlServiceForRNP44;
            case "РНП по 223-ФЗ":
                return xmlServiceForRNP223;
            default:
                return null;
        }

    }
}
